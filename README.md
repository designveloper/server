# MEKONG DEPOT BACKEND
## Description
This is server side implementation of Mekong Depot project.
## Technique
This project has been built and maintained using [Go](https://golang.org/) programming language and [Postgresql](https://www.postgresql.org/) as the main database management system.

Beside that, we also used some usefull open sourse framework/libraries to booted up this project which are listed bellow:

| Name       | Description and Link                                                                 |
| ---------- | ------------------------------------------------------------------------------------ |
| Gin        | [gin-gonic](https://gin-gonic.com/) to handle HTTP request                           |
| dbx        | [go-ozzo/dbx](https://github.com/go-ozzo/ozzo-dbx) to work with Database             |
| validation | [go-ozzo/ozzo-validation](https://github.com/go-ozzo/ozzo-validation) validate model |
| CircleCI   | [CircleCI](https://circleci.com/) to implement CI progress                           |
| Docker Hub | [Docker Hub](https://hub.docker.com)   to store docker image                         |
| Glide      | [Glide](https://github.com/Masterminds/glide) to manage go packages                  |
| dbmate     | [dbmate](https://github.com/turnitin/dbmate) a tool to run database migration files  |

## Installation
### Source code
Clone this repository into the directory named $GOPATH/src/bitbucket.com/designveloper/mekong-api
### Install Dependencies
run `glide instsall`

### Creating Database schema (Local development only)
**Note**: This app reads all configuration from a `yaml` file, these info maybe changed in production environment.
to start working in local, please create a directory named `config` then create an `app.yaml` file inside of it.

example content:
```yaml
database_url: "postgres://postgres@postgres:5432/mekong-depot?sslmode=disable"

smtp_mail: "mekongdepot.dev@gmail.com"
smtp_password: "mekongdepot100"
smtp_server: "smtp.gmail.com"
smtp_port: 587
```

## Development Guide
This project uses 3-tiers infracstructure which include 3 difference layers (`api`, `service` and `repository`) in `domain` folder.

This project use a `core` module(`core` folder) as a submodule git.
to init and pull submodule, rum command: `git submodule update --init`

## Coding Style
The coding style follows the go standard format. This project was already set up a script to install `pre-commit` hook of git which will check all the developer's commits, it will help the coding style better.

## Continueous Integrate and Continueous Delivery
- **CI**: Uses CircleCI for building a workflow which include 2 main progresses: Code building and Test running(not implemented yet).
  After the code buidling successed, it will already has a completed new version of code which store in docker hub.
- **CD** the deliver progess pulls the latest code from docker hub and deploy a new container version in Kubernete.


