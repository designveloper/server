-- migrate:up
-- INSERT DATA FOR FACET
INSERT INTO public.facet(id, name, data_type, default_value_text) VALUES (1, 'Age Group', 'text', 'Adults');
INSERT INTO public.facet(id, name, data_type, default_value_text) VALUES (2, 'Dress Type', 'text', 'Casual Dresses');
INSERT INTO public.facet(id, name, data_type, default_value_text) VALUES (3, 'Pattern Type', 'text', 'Solid');
INSERT INTO public.facet(id, name, data_type, default_value_text) VALUES (4, 'Waistline', 'text', 'Empire');
INSERT INTO public.facet(id, name, data_type, default_value_text) VALUES (5, 'Neckline', 'text', 'V-neck');
INSERT INTO public.facet(id, name, data_type, default_value_text) VALUES (6, 'Dresses Length', 'text', 'Mid-Calf');
INSERT INTO public.facet(id, name, data_type, default_value_text) VALUES (7, 'Style', 'text', 'Bohemian');
INSERT INTO public.facet(id, name, data_type, default_value_text) VALUES (8, 'Sihouette', 'text', 'Fit and Flare');
INSERT INTO public.facet(id, name, data_type, default_value_text) VALUES (9, 'Season', 'text', 'Summer');
INSERT INTO public.facet(id, name, data_type, default_value_text) VALUES (10, 'Sleeve Length', 'text', 'Sleeveless');
INSERT INTO public.facet(id, name, data_type, default_value_text) VALUES (11, 'Decoration', 'text', 'Button, Draped, Pockets');
INSERT INTO public.facet(id, name, data_type, default_value_text) VALUES (12, 'Supply Type', 'text', 'In-Stock Items');
INSERT INTO public.facet(id, name, data_type, default_value_text) VALUES (13, 'Technics', 'text', 'Printed');
INSERT INTO public.facet(id, name, data_type, default_value_text) VALUES (14, 'Material', 'text', 'Spandex / Cotton');
INSERT INTO public.facet(id, name, data_type, default_value_text) VALUES (15, 'Fabric Type', 'text', 'Woven');
INSERT INTO public.facet(id, name, data_type, default_value_text) VALUES (16, 'Feature', 'text', 'Breathable');
INSERT INTO public.facet(id, name, data_type, default_value_text) VALUES (17, 'Place of Origin', 'text', 'Guangdong, China');
INSERT INTO public.facet(id, name, data_type, default_value_text) VALUES (18, 'Brand Name', 'text', 'OOTN');
INSERT INTO public.facet(id, name, data_type, default_value_text) VALUES (19, 'Model Number', 'text', 'LYQ6178');
INSERT INTO public.facet(id, name, data_type, default_value_text) VALUES (20, 'Sleeve Style', 'text', 'Sleeveless');

-- update facet serial id to current value
ALTER SEQUENCE facet_id_seq RESTART WITH 21;

-- INSERT DATA FOR FACET_SET
INSERT INTO public.facet_set(category_id, facet_id) VALUES (4, 1);
INSERT INTO public.facet_set(category_id, facet_id) VALUES (4, 2);
INSERT INTO public.facet_set(category_id, facet_id) VALUES (4, 3);
INSERT INTO public.facet_set(category_id, facet_id) VALUES (4, 4);
INSERT INTO public.facet_set(category_id, facet_id) VALUES (4, 5);
INSERT INTO public.facet_set(category_id, facet_id) VALUES (4, 6);
INSERT INTO public.facet_set(category_id, facet_id) VALUES (4, 7);
INSERT INTO public.facet_set(category_id, facet_id) VALUES (4, 8);
INSERT INTO public.facet_set(category_id, facet_id) VALUES (4, 9);
INSERT INTO public.facet_set(category_id, facet_id) VALUES (4, 10);
INSERT INTO public.facet_set(category_id, facet_id) VALUES (4, 11);
INSERT INTO public.facet_set(category_id, facet_id) VALUES (4, 12);
INSERT INTO public.facet_set(category_id, facet_id) VALUES (4, 13);
INSERT INTO public.facet_set(category_id, facet_id) VALUES (4, 14);
INSERT INTO public.facet_set(category_id, facet_id) VALUES (4, 15);
INSERT INTO public.facet_set(category_id, facet_id) VALUES (4, 16);
INSERT INTO public.facet_set(category_id, facet_id) VALUES (4, 17);
INSERT INTO public.facet_set(category_id, facet_id) VALUES (4, 18);
INSERT INTO public.facet_set(category_id, facet_id) VALUES (4, 19);
INSERT INTO public.facet_set(category_id, facet_id) VALUES (4, 20);


-- migrate:down

