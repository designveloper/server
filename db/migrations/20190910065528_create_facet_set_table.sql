-- migrate:up
CREATE TABLE IF NOT EXISTS public.facet_set
(
  id         SERIAL PRIMARY KEY,
  name       VARCHAR(100),
  is_default BOOLEAN
);

CREATE TABLE IF NOT EXISTS public.facet
(
  id                     SERIAL PRIMARY KEY,
  name                   VARCHAR(255) NOT NULL,
  data_type              VARCHAR(255) NOT NULL,
  default_value_decimal  DECIMAL DEFAULT 0,
  default_value_text     TEXT    DEFAULT '',
  default_value_datetime TIMESTAMP WITH TIME ZONE,
  default_value_int      INTEGER DEFAULT 0,
  facet_set_id           INTEGER
    CONSTRAINT facet_set_data_facet_set_id_fkey REFERENCES public.facet_set
);

CREATE TABLE IF NOT EXISTS public.facet_data
(
  id           SERIAL PRIMARY KEY,
  facet_id INTEGER
    CONSTRAINT facet_data_facet_id_fkey REFERENCES public.facet
);

CREATE TABLE IF NOT EXISTS public.facet_data_value_text
(
  facet_data_id INTEGER,
  FOREIGN KEY (facet_data_id) REFERENCES public.facet_data (id),
  UNIQUE (facet_data_id),
  value         TEXT NOT NULL
);

CREATE TABLE IF NOT EXISTS public.facet_data_value_int
(
  facet_data_id INTEGER,
  FOREIGN KEY (facet_data_id) REFERENCES public.facet_data (id),
  UNIQUE (facet_data_id),
  value         INTEGER NOT NULL
);

CREATE TABLE IF NOT EXISTS public.product_facet
(
  id         SERIAL PRIMARY KEY,
  product_id INTEGER
    CONSTRAINT product_facet_product_id_fkey REFERENCES public.product,
  facet_id   INTEGER
    CONSTRAINT product_facet_facet_id_fkey REFERENCES public.facet,
  UNIQUE (product_id, facet_id)
);

-- attribute values in difference types
CREATE TABLE IF NOT EXISTS public.product_facet_value_decimal
(
  product_facet_id INTEGER,
  FOREIGN KEY (product_facet_id) REFERENCES public.product_facet (id),
  UNIQUE (product_facet_id),
  value            DECIMAL
);

CREATE TABLE IF NOT EXISTS public.product_facet_value_text
(
  product_facet_id INTEGER,
  FOREIGN KEY (product_facet_id) REFERENCES public.product_facet (id),
  UNIQUE (product_facet_id),
  value            TEXT
);

CREATE TABLE IF NOT EXISTS public.product_facet_value_datetime
(
  product_facet_id INTEGER,
  FOREIGN KEY (product_facet_id) REFERENCES public.product_facet (id),
  UNIQUE (product_facet_id),
  value            TIMESTAMP WITH TIME ZONE
);

CREATE TABLE IF NOT EXISTS public.product_facet_value_int
(
  product_facet_id INTEGER,
  FOREIGN KEY (product_facet_id) REFERENCES public.product_facet (id),
  UNIQUE (product_facet_id),
  value            VARCHAR(100)
);

-- create category_facet_set table
CREATE TABLE IF NOT EXISTS public.category_facet_set
(
  category_id  INTEGER
    CONSTRAINT category_facet_set_category_id_fkey REFERENCES public.category,
  facet_set_id INTEGER
    CONSTRAINT category_facet_set_facet_set_id_fkey REFERENCES public.facet_set,
  UNIQUE (category_id, facet_set_id)
);
-- migrate:down
DROP TABLE IF EXISTS public.facet;
DROP TABLE IF EXISTS public.facet_set;
DROP TABLE IF EXISTS facet_data;
DROP TABLE IF EXISTS public.product_facet;
DROP TABLE IF EXISTS public.product_facet_value_datetime;
DROP TABLE IF EXISTS public.product_facet_value_decimal;
DROP TABLE IF EXISTS public.product_facet_value_int;
DROP TABLE IF EXISTS public.product_facet_value_text;
DROP TABLE IF EXISTS public.category_facet_set;

