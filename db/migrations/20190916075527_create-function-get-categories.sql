-- migrate:up
CREATE OR REPLACE FUNCTION get_categories (n INTEGER)
    RETURNS TABLE (
                      ID INTEGER,
                      NAME VARCHAR,
                      CREATED_AT TIMESTAMP WITH TIME ZONE
                  ) as $$
DECLARE
    count INTEGER := 1 ;
    ids INTEGER[];
BEGIN
    ids := ARRAY (SELECT ARRAY_AGG(C.ID) FROM CATEGORY C WHERE C.PARENT_ID IS NULL);
    LOOP
        EXIT WHEN count = n ;
        count := count + 1;
        ids := ARRAY (SELECT ARRAY_AGG(C.ID) FROM CATEGORY C WHERE C.PARENT_ID = ANY (IDS));
    END LOOP ;

    RETURN QUERY (
        SELECT C.ID, C.NAME, C.CREATED_AT FROM CATEGORY C WHERE C.ID = ANY (ids)
    ) ;
END ;
$$ LANGUAGE plpgsql;

-- migrate:down
DROP FUNCTION IF EXISTS get_categories
