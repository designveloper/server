-- migrate:up
ALTER TABLE public.product_keyword
  ADD COLUMN keyword_vectors tsvector;

CREATE INDEX product_keyword_vectors_idx ON product_keyword USING GIN (keyword_vectors);

UPDATE public.product_keyword
SET keyword_vectors = (to_tsvector(keyword) || to_tsvector(REGEXP_REPLACE(keyword, '\s+', '', 'g')));
-- migrate:down
ALTER TABLE public.product_keyword
  DROP COLUMN IF EXISTS keyword_vectors;

DROP INDEX IF EXISTS category_keyword_vectors_idx;

