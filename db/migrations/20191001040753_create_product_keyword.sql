-- migrate:up
CREATE TABLE IF NOT EXISTS product_keyword
(
  product_id INTEGER
    CONSTRAINT product_keyword_product_id_fk REFERENCES public.product,
  keyword    TEXT NOT NULL
)

-- migrate:down

