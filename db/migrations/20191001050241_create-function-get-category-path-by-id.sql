-- migrate:up
CREATE OR REPLACE FUNCTION get_category_path (catID INTEGER)
    RETURNS TABLE (
                      ID INTEGER,
                      NAME VARCHAR,
                      CREATED_AT TIMESTAMP WITH TIME ZONE
                  ) as $$
DECLARE
    parentID INTEGER;
    ids INTEGER[];
BEGIN
    ids := ids || catID;
    SELECT c.PARENT_ID INTO parentID FROM CATEGORY c WHERE c.ID = catID;
    ids := ids || parentID;
    LOOP
        EXIT WHEN parentID IS NULL;
        SELECT c.PARENT_ID INTO parentID FROM CATEGORY c WHERE c.ID = parentID;
        ids := ids || parentID;
    END LOOP ;

    RETURN QUERY (
        SELECT C.ID, C.NAME, C.CREATED_AT FROM CATEGORY C WHERE C.ID = ANY (ids)
    ) ;
END ;
$$ LANGUAGE plpgsql;

-- migrate:down
DROP FUNCTION IF EXISTS get_category_path;
