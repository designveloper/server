-- migrate:up
CREATE SEQUENCE public.category_serial_id
  START WITH 1000
  INCREMENT BY 1
  NO MINVALUE
  NO MAXVALUE;

-- create category table
CREATE TABLE IF NOT EXISTS public.category
(
  id         INTEGER DEFAULT nextval('public.category_serial_id'::regclass) NOT NULL PRIMARY KEY,
  name       VARCHAR(255)                                                   NOT NULL,
  parent_id  INTEGER
    CONSTRAINT category_parent_id_fkey REFERENCES public.category,
  created_at TIMESTAMP WITH TIME ZONE                                       NOT NULL,
  updated_at TIMESTAMP WITH TIME ZONE
);

-- create category_product table
CREATE TABLE IF NOT EXISTS public.category_product
(
  category_id INTEGER
    CONSTRAINT category_product_category_id_fkey REFERENCES public.category,
  product_id  INTEGER
    CONSTRAINT category_product_product_id_fkey REFERENCES public.product,
  UNIQUE (category_id, product_id)
);

-- migrate:down
DROP TABLE IF EXISTS public.category;
DROP TABLE IF EXISTS public.category_product;
