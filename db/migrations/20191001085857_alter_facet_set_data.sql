-- migrate:up
ALTER TABLE public.facet_data_value_text ALTER COLUMN facet_data_id SET NOT NULL;
ALTER TABLE public.facet_data_value_int ALTER COLUMN facet_data_id SET NOT NULL;
-- migrate:down

