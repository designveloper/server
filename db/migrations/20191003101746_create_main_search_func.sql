-- migrate:up
create or replace function main_search(keysearch text)
  returns TABLE
          (
            result jsonb
          )
  language plpgsql
as
$$
BEGIN
  RETURN QUERY
    SELECT jsonb_build_object('id', product.id, 'search', product.name, 'type', 'p', 'category',
                              jsonb_build_object('id', c.id, 'category', c.name)) result
    FROM product
           INNER JOIN category_product cp on product.id = cp.product_id
           INNER JOIN category c on cp.category_id = c.id
           LEFT JOIN product_keyword pk on product.id = pk.product_id
    WHERE product.name_vectors @@ plainto_tsquery(keysearch)
       OR product.name_vectors @@ to_tsquery(concat(regexp_replace(keysearch, '\s+', '', 'g'), ':*'))
       OR pk.keyword_vectors @@ plainto_tsquery(keysearch)
       OR pk.keyword_vectors @@ to_tsquery(concat(regexp_replace(keysearch, '\s+', '', 'g'), ':*'))

    UNION

    SELECT jsonb_build_object('id', c.id, 'search', c.name, 'type', 'c', 'category',
                              jsonb_build_object('id', cfc.id, 'category', cfc.name)) result
    FROM category c
           LEFT JOIN category cfc on c.parent_id = cfc.id
    WHERE c.name_vectors @@ plainto_tsquery(keysearch) --original keyword
       OR c.name_vectors @@ to_tsquery(concat(regexp_replace(keysearch, '\s+', '', 'g'), ':*')); -- partial keyword search
END;
$$;

-- migrate:down
drop function if exists main_search;