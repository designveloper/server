-- migrate:up
INSERT INTO product(sku, name, description, brand_id, price_id, quantity, taxable, created_at, updated_at, dep, path)
VALUES ('MD098532', 'Alice Mustard Crochet Top Midaxi Dress With Pleated', 'One-shoulder draped silk-satin mini dress',
        NULL, NULL, 3455, true, '2019-09-19 10:31:26.922598', NULL, 4, 4);
INSERT INTO product(sku, name, description, brand_id, price_id, quantity, taxable, created_at, updated_at, dep, path)
VALUES ('MD083580', 'Anchorace Polka-dot Georgette Mini Wrap Dress', 'One-shoulder draped silk-satin mini dress', NULL,
        NULL, 3759, false, '2019-09-19 10:31:26.922598', NULL, 4, 4);
INSERT INTO product(sku, name, description, brand_id, price_id, quantity, taxable, created_at, updated_at, dep, path)
VALUES ('MD071854', 'Animal Print Oversize Batwing Shift Dress', 'One-shoulder draped silk-satin mini dress', NULL,
        NULL, 2497, true, '2019-09-19 10:31:26.922598', NULL, 4, 4);
INSERT INTO product(sku, name, description, brand_id, price_id, quantity, taxable, created_at, updated_at, dep, path)
VALUES ('MD047741', 'Basic Strappy Cami Bodycon Dress', 'One-shoulder draped silk-satin mini dress', NULL, NULL, 1694,
        true, '2019-09-19 10:31:26.922598', NULL, 4, 4);
INSERT INTO product(sku, name, description, brand_id, price_id, quantity, taxable, created_at, updated_at, dep, path)
VALUES ('MD069448', 'Belt shirt dress', 'One-shoulder draped silk-satin mini dress', NULL, NULL, 4324, false,
        '2019-09-19 10:31:26.922598', NULL, 4, 4);
INSERT INTO product(sku, name, description, brand_id, price_id, quantity, taxable, created_at, updated_at, dep, path)
VALUES ('MD080299', 'Belted Check Shift Dress', 'One-shoulder draped silk-satin mini dress', NULL, NULL, 1807, false,
        '2019-09-19 10:31:26.922598', NULL, 4, 4);
INSERT INTO product(sku, name, description, brand_id, price_id, quantity, taxable, created_at, updated_at, dep, path)
VALUES ('MD018415', 'Belted Floral Schiffli Cotton Midi Dress', 'One-shoulder draped silk-satin mini dress', NULL, NULL,
        3341, false, '2019-09-19 10:31:26.922598', NULL, 4, 4);
INSERT INTO product(sku, name, description, brand_id, price_id, quantity, taxable, created_at, updated_at, dep, path)
VALUES ('MD077049', 'Belted Maxi Dress', 'One-shoulder draped silk-satin mini dress', NULL, NULL, 129, false,
        '2019-09-19 10:31:26.922598', NULL, 4, 4);
INSERT INTO product(sku, name, description, brand_id, price_id, quantity, taxable, created_at, updated_at, dep, path)
VALUES ('MD016585', 'Belted Split Midi T Shirt Dress', 'One-shoulder draped silk-satin mini dress', NULL, NULL, 4337,
        false, '2019-09-19 10:31:26.922598', NULL, 4, 4);
INSERT INTO product(sku, name, description, brand_id, price_id, quantity, taxable, created_at, updated_at, dep, path)
VALUES ('MD069572', 'Belted Woven Midi Dress', 'One-shoulder draped silk-satin mini dress', NULL, NULL, 745, true,
        '2019-09-19 10:31:26.922598', NULL, 4, 4);
INSERT INTO product(sku, name, description, brand_id, price_id, quantity, taxable, created_at, updated_at, dep, path)
VALUES ('MD084797', 'Boho Crochet Detail Wide Sleeve Smock Dress', 'One-shoulder draped silk-satin mini dress', NULL,
        NULL, 902, false, '2019-09-19 10:31:26.922598', NULL, 4, 4);
INSERT INTO product(sku, name, description, brand_id, price_id, quantity, taxable, created_at, updated_at, dep, path)
VALUES ('MD011983', 'Cap Sleeve Shirred Waist Floral Maxi Dress', 'One-shoulder draped silk-satin mini dress', NULL,
        NULL, 3268, false, '2019-09-19 10:31:26.922598', NULL, 4, 4);
INSERT INTO product(sku, name, description, brand_id, price_id, quantity, taxable, created_at, updated_at, dep, path)
VALUES ('MD052163', 'Checked V-Neck Wrap D Ring Detail Skater Dress', 'One-shoulder draped silk-satin mini dress', NULL,
        NULL, 4550, false, '2019-09-19 10:31:26.922598', NULL, 4, 4);
INSERT INTO product(sku, name, description, brand_id, price_id, quantity, taxable, created_at, updated_at, dep, path)
VALUES ('MD088732', 'Crepe Ruffle Wrap Midi Skater Dress', 'One-shoulder draped silk-satin mini dress', NULL, NULL,
        4440, false, '2019-09-19 10:31:26.922598', NULL, 4, 4);
INSERT INTO product(sku, name, description, brand_id, price_id, quantity, taxable, created_at, updated_at, dep, path)
VALUES ('MD059801', 'Crew Neck Knot Front Puff Sleeve Shift Dress', 'One-shoulder draped silk-satin mini dress', NULL,
        NULL, 1776, false, '2019-09-19 10:31:26.922598', NULL, 4, 4);
INSERT INTO product(sku, name, description, brand_id, price_id, quantity, taxable, created_at, updated_at, dep, path)
VALUES ('MD067239', 'Crinkle One Shoulder Mini Dress', 'One-shoulder draped silk-satin mini dress', NULL, NULL, 4366,
        false, '2019-09-19 10:31:26.922598', NULL, 4, 4);
INSERT INTO product(sku, name, description, brand_id, price_id, quantity, taxable, created_at, updated_at, dep, path)
VALUES ('MD046793', 'Crossover Ribbed Wool-blend Turtleneck Mini Dress', 'One-shoulder draped silk-satin mini dress',
        NULL, NULL, 159, true, '2019-09-19 10:31:26.922598', NULL, 4, 4);
INSERT INTO product(sku, name, description, brand_id, price_id, quantity, taxable, created_at, updated_at, dep, path)
VALUES ('MD099167', 'Cutout Draped Stretch-jersey Gown', 'One-shoulder draped silk-satin mini dress', NULL, NULL, 3426,
        true, '2019-09-19 10:31:26.922598', NULL, 4, 4);
INSERT INTO product(sku, name, description, brand_id, price_id, quantity, taxable, created_at, updated_at, dep, path)
VALUES ('MD083433', 'Ditsy Floral Ruffle Sundress', 'One-shoulder draped silk-satin mini dress', NULL, NULL, 1999,
        false, '2019-09-19 10:31:26.922598', NULL, 4, 4);
INSERT INTO product(sku, name, description, brand_id, price_id, quantity, taxable, created_at, updated_at, dep, path)
VALUES ('MD018825', 'Draped Satin-crepe And Striped Organza Midi Dress', 'One-shoulder draped silk-satin mini dress',
        NULL, NULL, 1430, false, '2019-09-19 10:31:26.922598', NULL, 4, 4);
INSERT INTO product(sku, name, description, brand_id, price_id, quantity, taxable, created_at, updated_at, dep, path)
VALUES ('MD014174', 'Echos Of Enchantment Crystal-embellished Silk Dress', 'One-shoulder draped silk-satin mini dress',
        NULL, NULL, 4833, true, '2019-09-19 10:31:26.922598', NULL, 4, 4);
INSERT INTO product(sku, name, description, brand_id, price_id, quantity, taxable, created_at, updated_at, dep, path)
VALUES ('MD011417', 'Ella Pleated Printed Cotton-poplin Mini Dress', 'One-shoulder draped silk-satin mini dress', NULL,
        NULL, 4607, false, '2019-09-19 10:31:26.922598', NULL, 4, 4);
INSERT INTO product(sku, name, description, brand_id, price_id, quantity, taxable, created_at, updated_at, dep, path)
VALUES ('MD056933', 'Embroidery striped dress', 'One-shoulder draped silk-satin mini dress', NULL, NULL, 2923, false,
        '2019-09-19 10:31:26.922598', NULL, 4, 4);
INSERT INTO product(sku, name, description, brand_id, price_id, quantity, taxable, created_at, updated_at, dep, path)
VALUES ('MD052120', 'Flared Sleeve Ruched Detail Mini Dress', 'One-shoulder draped silk-satin mini dress', NULL, NULL,
        1950, false, '2019-09-19 10:31:26.922598', NULL, 4, 4);
INSERT INTO product(sku, name, description, brand_id, price_id, quantity, taxable, created_at, updated_at, dep, path)
VALUES ('MD066100', 'Floral Wrap Skater Dress', 'One-shoulder draped silk-satin mini dress', NULL, NULL, 4338, true,
        '2019-09-19 10:31:26.922598', NULL, 4, 4);
INSERT INTO product(sku, name, description, brand_id, price_id, quantity, taxable, created_at, updated_at, dep, path)
VALUES ('MD044114', 'Florence Satin Cowl Neck Bodycon Dress', 'One-shoulder draped silk-satin mini dress', NULL, NULL,
        1083, true, '2019-09-19 10:31:26.922598', NULL, 4, 4);
INSERT INTO product(sku, name, description, brand_id, price_id, quantity, taxable, created_at, updated_at, dep, path)
VALUES ('MD054744', 'Flowers cotton dress', 'One-shoulder draped silk-satin mini dress', NULL, NULL, 2083, true,
        '2019-09-19 10:31:26.922598', NULL, 4, 4);
INSERT INTO product(sku, name, description, brand_id, price_id, quantity, taxable, created_at, updated_at, dep, path)
VALUES ('MD041792', 'Funnel Neck Shift Dress', 'One-shoulder draped silk-satin mini dress', NULL, NULL, 3284, false,
        '2019-09-19 10:31:26.922598', NULL, 4, 4);
INSERT INTO product(sku, name, description, brand_id, price_id, quantity, taxable, created_at, updated_at, dep, path)
VALUES ('MD032917', 'High Neck Flutter Sleeve Skater Dress', 'One-shoulder draped silk-satin mini dress', NULL, NULL,
        4708, true, '2019-09-19 10:31:26.922598', NULL, 4, 4);
INSERT INTO product(sku, name, description, brand_id, price_id, quantity, taxable, created_at, updated_at, dep, path)
VALUES ('MD058562', 'High Neck Ruffle Front Snake Midi Dress', 'One-shoulder draped silk-satin mini dress', NULL, NULL,
        3071, false, '2019-09-19 10:31:26.922598', NULL, 4, 4);
INSERT INTO product(sku, name, description, brand_id, price_id, quantity, taxable, created_at, updated_at, dep, path)
VALUES ('MD052140', 'High Neck Stretch Satin Midi Dress', 'One-shoulder draped silk-satin mini dress', NULL, NULL, 1207,
        false, '2019-09-19 10:31:26.922598', NULL, 4, 4);
INSERT INTO product(sku, name, description, brand_id, price_id, quantity, taxable, created_at, updated_at, dep, path)
VALUES ('MD099590', 'Ilana Shirred Faille Midi Dress', 'One-shoulder draped silk-satin mini dress', NULL, NULL, 1888,
        true, '2019-09-19 10:31:26.922598', NULL, 4, 4);
INSERT INTO product(sku, name, description, brand_id, price_id, quantity, taxable, created_at, updated_at, dep, path)
VALUES ('MD048864', 'Jacquard Midi Dress', 'One-shoulder draped silk-satin mini dress', NULL, NULL, 1114, true,
        '2019-09-19 10:31:26.922598', NULL, 4, 4);
INSERT INTO product(sku, name, description, brand_id, price_id, quantity, taxable, created_at, updated_at, dep, path)
VALUES ('MD014099', 'Jordi Dandelion Print Midi Dress', 'One-shoulder draped silk-satin mini dress', NULL, NULL, 4455,
        false, '2019-09-19 10:31:26.922598', NULL, 4, 4);
INSERT INTO product(sku, name, description, brand_id, price_id, quantity, taxable, created_at, updated_at, dep, path)
VALUES ('MD076663', 'Lace Panel High Neck Ruffle Hem Mini Dress', 'One-shoulder draped silk-satin mini dress', NULL,
        NULL, 3228, false, '2019-09-19 10:31:26.922598', NULL, 4, 4);
INSERT INTO product(sku, name, description, brand_id, price_id, quantity, taxable, created_at, updated_at, dep, path)
VALUES ('MD056033', 'Layered Satin-crepe Maxi Dress', 'One-shoulder draped silk-satin mini dress', NULL, NULL, 1777,
        false, '2019-09-19 10:31:26.922598', NULL, 4, 4);
INSERT INTO product(sku, name, description, brand_id, price_id, quantity, taxable, created_at, updated_at, dep, path)
VALUES ('MD082009', 'Leopard Geo Print Ruffle Sleeve Midaxi Dress', 'One-shoulder draped silk-satin mini dress', NULL,
        NULL, 4886, false, '2019-09-19 10:31:26.922598', NULL, 4, 4);
INSERT INTO product(sku, name, description, brand_id, price_id, quantity, taxable, created_at, updated_at, dep, path)
VALUES ('MD012117', 'Longline Square Neck Bodycon Dress', 'One-shoulder draped silk-satin mini dress', NULL, NULL, 881,
        false, '2019-09-19 10:31:26.922598', NULL, 4, 4);
INSERT INTO product(sku, name, description, brand_id, price_id, quantity, taxable, created_at, updated_at, dep, path)
VALUES ('MD082907', 'Mancora Midi Dress', 'One-shoulder draped silk-satin mini dress', NULL, NULL, 4308, false,
        '2019-09-19 10:31:26.922598', NULL, 4, 4);
INSERT INTO product(sku, name, description, brand_id, price_id, quantity, taxable, created_at, updated_at, dep, path)
VALUES ('MD051832', 'Michelle Asymmetric Ribbed Cotton-blend Midi Dress', 'One-shoulder draped silk-satin mini dress',
        NULL, NULL, 3709, false, '2019-09-19 10:31:26.922598', NULL, 4, 4);
INSERT INTO product(sku, name, description, brand_id, price_id, quantity, taxable, created_at, updated_at, dep, path)
VALUES ('MD050749', 'Micro Ditsy Button Pocket Smock Dress', 'One-shoulder draped silk-satin mini dress', NULL, NULL,
        431, false, '2019-09-19 10:31:26.922598', NULL, 4, 4);
INSERT INTO product(sku, name, description, brand_id, price_id, quantity, taxable, created_at, updated_at, dep, path)
VALUES ('MD082600', 'Moun Belted Vegan Leather Wrap-effect Midi Dress -', 'One-shoulder draped silk-satin mini dress',
        NULL, NULL, 1396, false, '2019-09-19 10:31:26.922598', NULL, 4, 4);
INSERT INTO product(sku, name, description, brand_id, price_id, quantity, taxable, created_at, updated_at, dep, path)
VALUES ('MD086956', 'Mustard Wrap Dress', 'One-shoulder draped silk-satin mini dress', NULL, NULL, 3976, true,
        '2019-09-19 10:31:26.922598', NULL, 4, 4);
INSERT INTO product(sku, name, description, brand_id, price_id, quantity, taxable, created_at, updated_at, dep, path)
VALUES ('MD018155', 'Myah Mini Dress', 'One-shoulder draped silk-satin mini dress', NULL, NULL, 789, false,
        '2019-09-19 10:31:26.922598', NULL, 4, 4);
INSERT INTO product(sku, name, description, brand_id, price_id, quantity, taxable, created_at, updated_at, dep, path)
VALUES ('MD015798', 'Nackiyé Nackiye Belted Velvet Midi Dress', 'One-shoulder draped silk-satin mini dress', NULL, NULL,
        4984, true, '2019-09-19 10:31:26.922598', NULL, 4, 4);
INSERT INTO product(sku, name, description, brand_id, price_id, quantity, taxable, created_at, updated_at, dep, path)
VALUES ('MD045722', 'Noe Mustard Frill Belted Pencil Dress', 'One-shoulder draped silk-satin mini dress', NULL, NULL,
        4606, false, '2019-09-19 10:31:26.922598', NULL, 4, 4);
INSERT INTO product(sku, name, description, brand_id, price_id, quantity, taxable, created_at, updated_at, dep, path)
VALUES ('MD024221', 'Notch Front Jumbo Ribbed Midi Dress', 'One-shoulder draped silk-satin mini dress', NULL, NULL,
        1629, false, '2019-09-19 10:31:26.922598', NULL, 4, 4);
INSERT INTO product(sku, name, description, brand_id, price_id, quantity, taxable, created_at, updated_at, dep, path)
VALUES ('MD093234', 'Off The Shoulder Jersey Swing Dress', 'One-shoulder draped silk-satin mini dress', NULL, NULL,
        2558, true, '2019-09-19 10:31:26.922598', NULL, 4, 4);
INSERT INTO product(sku, name, description, brand_id, price_id, quantity, taxable, created_at, updated_at, dep, path)
VALUES ('MD078066', 'One-shoulder Draped Silk-satin Mini Dress', 'One-shoulder draped silk-satin mini dress', NULL,
        NULL, 1763, true, '2019-09-19 10:31:26.922598', NULL, 4, 4);
INSERT INTO product(sku, name, description, brand_id, price_id, quantity, taxable, created_at, updated_at, dep, path)
VALUES ('MD091467', 'Open-back Metallic Crochet-knit Gown', 'One-shoulder draped silk-satin mini dress', NULL, NULL,
        4467, false, '2019-09-19 10:31:26.922598', NULL, 4, 4);
INSERT INTO product(sku, name, description, brand_id, price_id, quantity, taxable, created_at, updated_at, dep, path)
VALUES ('MD074622', 'Outlet Mustard Fluted Dress', 'One-shoulder draped silk-satin mini dress', NULL, NULL, 1925, true,
        '2019-09-19 10:31:26.922598', NULL, 4, 4);
INSERT INTO product(sku, name, description, brand_id, price_id, quantity, taxable, created_at, updated_at, dep, path)
VALUES ('MD024341', 'Outlet Yellow Daisy Crochet Dress', 'One-shoulder draped silk-satin mini dress', NULL, NULL, 2349,
        false, '2019-09-19 10:31:26.922598', NULL, 4, 4);
INSERT INTO product(sku, name, description, brand_id, price_id, quantity, taxable, created_at, updated_at, dep, path)
VALUES ('MD083263', 'Oversized Soft Knit Cowl Neck Jumper Dress', 'One-shoulder draped silk-satin mini dress', NULL,
        NULL, 356, false, '2019-09-19 10:31:26.922598', NULL, 4, 4);
INSERT INTO product(sku, name, description, brand_id, price_id, quantity, taxable, created_at, updated_at, dep, path)
VALUES ('MD035767', 'Petite Alice Mustard Crochet Top Midaxi Dress With', 'One-shoulder draped silk-satin mini dress',
        NULL, NULL, 3408, false, '2019-09-19 10:31:26.922598', NULL, 4, 4);
INSERT INTO product(sku, name, description, brand_id, price_id, quantity, taxable, created_at, updated_at, dep, path)
VALUES ('MD095031', 'Pleated V Neck Frill Hem Skater Dress', 'One-shoulder draped silk-satin mini dress', NULL, NULL,
        4000, false, '2019-09-19 10:31:26.922598', NULL, 4, 4);
INSERT INTO product(sku, name, description, brand_id, price_id, quantity, taxable, created_at, updated_at, dep, path)
VALUES ('MD047045', 'Plisse Buckle Wrap Midi Dress', 'One-shoulder draped silk-satin mini dress', NULL, NULL, 510,
        false, '2019-09-19 10:31:26.922598', NULL, 4, 4);
INSERT INTO product(sku, name, description, brand_id, price_id, quantity, taxable, created_at, updated_at, dep, path)
VALUES ('MD034751', 'Plus Ditsy Floral Smock Dress', 'One-shoulder draped silk-satin mini dress', NULL, NULL, 2998,
        true, '2019-09-19 10:31:26.922598', NULL, 4, 4);
INSERT INTO product(sku, name, description, brand_id, price_id, quantity, taxable, created_at, updated_at, dep, path)
VALUES ('MD029044', 'Plus Double Layer Midi Dress', 'One-shoulder draped silk-satin mini dress', NULL, NULL, 1629, true,
        '2019-09-19 10:31:26.922598', NULL, 4, 4);
INSERT INTO product(sku, name, description, brand_id, price_id, quantity, taxable, created_at, updated_at, dep, path)
VALUES ('MD036071', 'Plus Floral Ruffle Wrap Midi Dress', 'One-shoulder draped silk-satin mini dress', NULL, NULL, 3050,
        true, '2019-09-19 10:31:26.922598', NULL, 4, 4);
INSERT INTO product(sku, name, description, brand_id, price_id, quantity, taxable, created_at, updated_at, dep, path)
VALUES ('MD059066', 'Plus Kimono Sleeve Wrap Over Midi Dress', 'One-shoulder draped silk-satin mini dress', NULL, NULL,
        2664, true, '2019-09-19 10:31:26.922598', NULL, 4, 4);
INSERT INTO product(sku, name, description, brand_id, price_id, quantity, taxable, created_at, updated_at, dep, path)
VALUES ('MD042878', 'Plus Plunge Kimono Sleeve Skater Dress', 'One-shoulder draped silk-satin mini dress', NULL, NULL,
        1864, true, '2019-09-19 10:31:26.922598', NULL, 4, 4);
INSERT INTO product(sku, name, description, brand_id, price_id, quantity, taxable, created_at, updated_at, dep, path)
VALUES ('MD043267', 'Plus Plunge Ruffle Belt Midi Dress', 'One-shoulder draped silk-satin mini dress', NULL, NULL, 193,
        false, '2019-09-19 10:31:26.922598', NULL, 4, 4);
INSERT INTO product(sku, name, description, brand_id, price_id, quantity, taxable, created_at, updated_at, dep, path)
VALUES ('MD092742', 'Plus Polka Dot Plunge Ruffle Skater Dress', 'One-shoulder draped silk-satin mini dress', NULL,
        NULL, 1393, false, '2019-09-19 10:31:26.922598', NULL, 4, 4);
INSERT INTO product(sku, name, description, brand_id, price_id, quantity, taxable, created_at, updated_at, dep, path)
VALUES ('MD074325', 'Plus Polka Dot Ruffle Wrap Midi Dress', 'One-shoulder draped silk-satin mini dress', NULL, NULL,
        1851, true, '2019-09-19 10:31:26.922598', NULL, 4, 4);
INSERT INTO product(sku, name, description, brand_id, price_id, quantity, taxable, created_at, updated_at, dep, path)
VALUES ('MD069310', 'Plus Puff Sleeve Smock Shirt Dress', 'One-shoulder draped silk-satin mini dress', NULL, NULL, 3705,
        false, '2019-09-19 10:31:26.922598', NULL, 4, 4);
INSERT INTO product(sku, name, description, brand_id, price_id, quantity, taxable, created_at, updated_at, dep, path)
VALUES ('MD045224', 'Plus Soft Rib Long Sleeve Skater Dress', 'One-shoulder draped silk-satin mini dress', NULL, NULL,
        1749, false, '2019-09-19 10:31:26.922598', NULL, 4, 4);
INSERT INTO product(sku, name, description, brand_id, price_id, quantity, taxable, created_at, updated_at, dep, path)
VALUES ('MD054422', 'Plus Strappy Knot Front Swing Dress', 'One-shoulder draped silk-satin mini dress', NULL, NULL, 902,
        false, '2019-09-19 10:31:26.922598', NULL, 4, 4);
INSERT INTO product(sku, name, description, brand_id, price_id, quantity, taxable, created_at, updated_at, dep, path)
VALUES ('MD041925', 'Plus Stripe Tie Neck Tiered Smock Dress', 'One-shoulder draped silk-satin mini dress', NULL, NULL,
        3145, true, '2019-09-19 10:31:26.922598', NULL, 4, 4);
INSERT INTO product(sku, name, description, brand_id, price_id, quantity, taxable, created_at, updated_at, dep, path)
VALUES ('MD028396', 'Plus Woven Floral Shift Dress', 'One-shoulder draped silk-satin mini dress', NULL, NULL, 1494,
        false, '2019-09-19 10:31:26.922598', NULL, 4, 4);
INSERT INTO product(sku, name, description, brand_id, price_id, quantity, taxable, created_at, updated_at, dep, path)
VALUES ('MD013319', 'Printed Wrap Dress', 'One-shoulder draped silk-satin mini dress', NULL, NULL, 229, false,
        '2019-09-19 10:31:26.922598', NULL, 4, 4);
INSERT INTO product(sku, name, description, brand_id, price_id, quantity, taxable, created_at, updated_at, dep, path)
VALUES ('MD069482', 'Printed short dress', 'One-shoulder draped silk-satin mini dress', NULL, NULL, 292, true,
        '2019-09-19 10:31:26.922598', NULL, 4, 4);
INSERT INTO product(sku, name, description, brand_id, price_id, quantity, taxable, created_at, updated_at, dep, path)
VALUES ('MD095336', 'Ribbed Cap Sleeved Bodycon Dress', 'One-shoulder draped silk-satin mini dress', NULL, NULL, 3103,
        false, '2019-09-19 10:31:26.922598', NULL, 4, 4);
INSERT INTO product(sku, name, description, brand_id, price_id, quantity, taxable, created_at, updated_at, dep, path)
VALUES ('MD039878', 'Ring Detail Sun Dress', 'One-shoulder draped silk-satin mini dress', NULL, NULL, 3632, true,
        '2019-09-19 10:31:26.922598', NULL, 4, 4);
INSERT INTO product(sku, name, description, brand_id, price_id, quantity, taxable, created_at, updated_at, dep, path)
VALUES ('MD053929', 'Rouche Front Frill Mini Dress', 'One-shoulder draped silk-satin mini dress', NULL, NULL, 1887,
        true, '2019-09-19 10:31:26.922598', NULL, 4, 4);
INSERT INTO product(sku, name, description, brand_id, price_id, quantity, taxable, created_at, updated_at, dep, path)
VALUES ('MD094279', 'Ruffle Angel Sleeve Bolo Tie Midi Dress', 'One-shoulder draped silk-satin mini dress', NULL, NULL,
        951, false, '2019-09-19 10:31:26.922598', NULL, 4, 4);
INSERT INTO product(sku, name, description, brand_id, price_id, quantity, taxable, created_at, updated_at, dep, path)
VALUES ('MD056378', 'Ruffle Detail V-Neck Dress', 'One-shoulder draped silk-satin mini dress', NULL, NULL, 2634, false,
        '2019-09-19 10:31:26.922598', NULL, 4, 4);
INSERT INTO product(sku, name, description, brand_id, price_id, quantity, taxable, created_at, updated_at, dep, path)
VALUES ('MD075609', 'Ruffle-trimmed Broderie Anglaise Crepe De Chine Midi', 'One-shoulder draped silk-satin mini dress',
        NULL, NULL, 781, false, '2019-09-19 10:31:26.922598', NULL, 4, 4);
INSERT INTO product(sku, name, description, brand_id, price_id, quantity, taxable, created_at, updated_at, dep, path)
VALUES ('MD032834', 'Ruffled Satin-crepe Midi Dress', 'One-shoulder draped silk-satin mini dress', NULL, NULL, 881,
        false, '2019-09-19 10:31:26.922598', NULL, 4, 4);
INSERT INTO product(sku, name, description, brand_id, price_id, quantity, taxable, created_at, updated_at, dep, path)
VALUES ('MD061453', 'Ruffled Satin-jersey Midi Dress', 'One-shoulder draped silk-satin mini dress', NULL, NULL, 751,
        true, '2019-09-19 10:31:26.922598', NULL, 4, 4);
INSERT INTO product(sku, name, description, brand_id, price_id, quantity, taxable, created_at, updated_at, dep, path)
VALUES ('MD062881', 'Satin Wrap Detail Dress', 'One-shoulder draped silk-satin mini dress', NULL, NULL, 781, true,
        '2019-09-19 10:31:26.922598', NULL, 4, 4);
INSERT INTO product(sku, name, description, brand_id, price_id, quantity, taxable, created_at, updated_at, dep, path)
VALUES ('MD034232', 'Satin gown', 'One-shoulder draped silk-satin mini dress', NULL, NULL, 2091, false,
        '2019-09-19 10:31:26.922598', NULL, 4, 4);
INSERT INTO product(sku, name, description, brand_id, price_id, quantity, taxable, created_at, updated_at, dep, path)
VALUES ('MD015239', 'Satin-crepe Maxi Slip Dress', 'One-shoulder draped silk-satin mini dress', NULL, NULL, 4393, true,
        '2019-09-19 10:31:26.922598', NULL, 4, 4);
INSERT INTO product(sku, name, description, brand_id, price_id, quantity, taxable, created_at, updated_at, dep, path)
VALUES ('MD080722', 'Scoop Front Jersey Maxi Dress', 'One-shoulder draped silk-satin mini dress', NULL, NULL, 549, true,
        '2019-09-19 10:31:26.922598', NULL, 4, 4);
INSERT INTO product(sku, name, description, brand_id, price_id, quantity, taxable, created_at, updated_at, dep, path)
VALUES ('MD037971', 'Sequined Silk-chiffon Slip Dress', 'One-shoulder draped silk-satin mini dress', NULL, NULL, 3400,
        false, '2019-09-19 10:31:26.922598', NULL, 4, 4);
INSERT INTO product(sku, name, description, brand_id, price_id, quantity, taxable, created_at, updated_at, dep, path)
VALUES ('MD077728', 'Shana Mustard Lace Bodycon Dress', 'One-shoulder draped silk-satin mini dress', NULL, NULL, 4902,
        true, '2019-09-19 10:31:26.922598', NULL, 4, 4);
INSERT INTO product(sku, name, description, brand_id, price_id, quantity, taxable, created_at, updated_at, dep, path)
VALUES ('MD041696', 'Short Sleeve Belted Skater Dress', 'One-shoulder draped silk-satin mini dress', NULL, NULL, 2563,
        false, '2019-09-19 10:31:26.922598', NULL, 4, 4);
INSERT INTO product(sku, name, description, brand_id, price_id, quantity, taxable, created_at, updated_at, dep, path)
VALUES ('MD062810', 'Short Sleeve Floral Print Shift Dress', 'One-shoulder draped silk-satin mini dress', NULL, NULL,
        789, true, '2019-09-19 10:31:26.922598', NULL, 4, 4);
INSERT INTO product(sku, name, description, brand_id, price_id, quantity, taxable, created_at, updated_at, dep, path)
VALUES ('MD041924', 'Silk-satin Gown', 'One-shoulder draped silk-satin mini dress', NULL, NULL, 3979, true,
        '2019-09-19 10:31:26.922598', NULL, 4, 4);
INSERT INTO product(sku, name, description, brand_id, price_id, quantity, taxable, created_at, updated_at, dep, path)
VALUES ('MD033863', 'Skinny Strap Tie Front Midi Dress', 'One-shoulder draped silk-satin mini dress', NULL, NULL, 882,
        true, '2019-09-19 10:31:26.922598', NULL, 4, 4);
INSERT INTO product(sku, name, description, brand_id, price_id, quantity, taxable, created_at, updated_at, dep, path)
VALUES ('MD040661', 'Sleeveless Tiered Midi Dress', 'One-shoulder draped silk-satin mini dress', NULL, NULL, 1979, true,
        '2019-09-19 10:31:26.922598', NULL, 4, 4);
INSERT INTO product(sku, name, description, brand_id, price_id, quantity, taxable, created_at, updated_at, dep, path)
VALUES ('MD019247', 'Smock Animal Print Dress', 'One-shoulder draped silk-satin mini dress', NULL, NULL, 1575, false,
        '2019-09-19 10:31:26.922598', NULL, 4, 4);
INSERT INTO product(sku, name, description, brand_id, price_id, quantity, taxable, created_at, updated_at, dep, path)
VALUES ('MD088165', 'Square Neck Midi Skater Dress', 'One-shoulder draped silk-satin mini dress', NULL, NULL, 187,
        false, '2019-09-19 10:31:26.922598', NULL, 4, 4);
INSERT INTO product(sku, name, description, brand_id, price_id, quantity, taxable, created_at, updated_at, dep, path)
VALUES ('MD027895', 'Strapless Draped Printed Silk-georgette Dress', 'One-shoulder draped silk-satin mini dress', NULL,
        NULL, 3078, true, '2019-09-19 10:31:26.922598', NULL, 4, 4);
INSERT INTO product(sku, name, description, brand_id, price_id, quantity, taxable, created_at, updated_at, dep, path)
VALUES ('MD080008', 'Striped Slub Linen-blend Jersey Tank', 'One-shoulder draped silk-satin mini dress', NULL, NULL,
        1437, false, '2019-09-19 10:31:26.922598', NULL, 4, 4);
INSERT INTO product(sku, name, description, brand_id, price_id, quantity, taxable, created_at, updated_at, dep, path)
VALUES ('MD089002', 'Tall Horn Button Detail Skater Dress', 'One-shoulder draped silk-satin mini dress', NULL, NULL,
        3395, false, '2019-09-19 10:31:26.922598', NULL, 4, 4);
INSERT INTO product(sku, name, description, brand_id, price_id, quantity, taxable, created_at, updated_at, dep, path)
VALUES ('MD054399', 'Tall Roll Neck Jumper Dress', 'One-shoulder draped silk-satin mini dress', NULL, NULL, 3409, false,
        '2019-09-19 10:31:26.922598', NULL, 4, 4);
INSERT INTO product(sku, name, description, brand_id, price_id, quantity, taxable, created_at, updated_at, dep, path)
VALUES ('MD085488', 'Tall Turn Cuff Oversized T-Shirt Dress', 'One-shoulder draped silk-satin mini dress', NULL, NULL,
        2557, false, '2019-09-19 10:31:26.922598', NULL, 4, 4);
INSERT INTO product(sku, name, description, brand_id, price_id, quantity, taxable, created_at, updated_at, dep, path)
VALUES ('MD066487', 'Taro Mustard Floral-Print Crochet Lace Dress', 'One-shoulder draped silk-satin mini dress', NULL,
        NULL, 1208, false, '2019-09-19 10:31:26.922598', NULL, 4, 4);
INSERT INTO product(sku, name, description, brand_id, price_id, quantity, taxable, created_at, updated_at, dep, path)
VALUES ('MD021063', 'Twill Midi Dress', 'One-shoulder draped silk-satin mini dress', NULL, NULL, 3514, true,
        '2019-09-19 10:31:26.922598', NULL, 4, 4);
INSERT INTO product(sku, name, description, brand_id, price_id, quantity, taxable, created_at, updated_at, dep, path)
VALUES ('MD026525', 'V Bar Strappy Midi Stretch Satin Dress', 'One-shoulder draped silk-satin mini dress', NULL, NULL,
        4545, false, '2019-09-19 10:31:26.922598', NULL, 4, 4);
INSERT INTO product(sku, name, description, brand_id, price_id, quantity, taxable, created_at, updated_at, dep, path)
VALUES ('MD083919', 'V-Neck Dress', 'One-shoulder draped silk-satin mini dress', NULL, NULL, 2023, false,
        '2019-09-19 10:31:26.922598', NULL, 4, 4);
INSERT INTO product(sku, name, description, brand_id, price_id, quantity, taxable, created_at, updated_at, dep, path)
VALUES ('MD081932', 'Womens Petite Idol Agadir Paisley Pleat Mini Shirt Dress',
        'One-shoulder draped silk-satin mini dress', NULL, NULL, 4318, true, '2019-09-19 10:31:26.922598', NULL, 4, 4);
INSERT INTO product(sku, name, description, brand_id, price_id, quantity, taxable, created_at, updated_at, dep, path)
VALUES ('MD067832', 'Womens Petite Jacquard Belted Pinafore Dress', 'One-shoulder draped silk-satin mini dress', NULL,
        NULL, 3765, true, '2019-09-19 10:31:26.922598', NULL, 4, 4);
INSERT INTO product(sku, name, description, brand_id, price_id, quantity, taxable, created_at, updated_at, dep, path)
VALUES ('MD084755', 'Woven Floral Chain Midi Tea Dress', 'One-shoulder draped silk-satin mini dress', NULL, NULL, 1835,
        true, '2019-09-19 10:31:26.922598', NULL, 4, 4);
INSERT INTO product(sku, name, description, brand_id, price_id, quantity, taxable, created_at, updated_at, dep, path)
VALUES ('MD067564', 'Woven Floral Keyhole Shift Dress', 'One-shoulder draped silk-satin mini dress', NULL, NULL, 3420,
        true, '2019-09-19 10:31:26.922598', NULL, 4, 4);
INSERT INTO product(sku, name, description, brand_id, price_id, quantity, taxable, created_at, updated_at, dep, path)
VALUES ('MD099860', 'Woven Patch Work Print Shirred Sundress', 'One-shoulder draped silk-satin mini dress', NULL, NULL,
        1137, true, '2019-09-19 10:31:26.922598', NULL, 4, 4);
INSERT INTO product(sku, name, description, brand_id, price_id, quantity, taxable, created_at, updated_at, dep, path)
VALUES ('MD084140', 'Woven Stripe Wrap Mini Dress', 'One-shoulder draped silk-satin mini dress', NULL, NULL, 2235, true,
        '2019-09-19 10:31:26.922598', NULL, 4, 4);
INSERT INTO product(sku, name, description, brand_id, price_id, quantity, taxable, created_at, updated_at, dep, path)
VALUES ('MD073925', 'Wrap Zebra Buckle Detail Mini Dress', 'One-shoulder draped silk-satin mini dress', NULL, NULL, 807,
        false, '2019-09-19 10:31:26.922598', NULL, 4, 4);
INSERT INTO product(sku, name, description, brand_id, price_id, quantity, taxable, created_at, updated_at, dep, path)
VALUES ('MD075048', 'TEST DATA', 'One-shoulder draped silk-satin mini dress', NULL, NULL, 1199, false,
        '2019-09-19 10:31:26.922598', NULL, 4, 4);

-- migrate:down

