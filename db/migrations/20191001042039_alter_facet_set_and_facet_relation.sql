-- migrate:up
ALTER TABLE public.facet
  DROP CONSTRAINT IF EXISTS facet_set_data_facet_set_id_fkey,
  DROP COLUMN IF EXISTS facet_set_id;

ALTER TABLE public.facet_set
  ADD COLUMN facet_id INTEGER CONSTRAINT facet_set_facet_id_fkey REFERENCES public.facet;
-- migrate:down

