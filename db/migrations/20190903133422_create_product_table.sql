-- migrate:up
CREATE TABLE IF NOT EXISTS public.brand
(
  id   SERIAL PRIMARY KEY,
  name VARCHAR(255)
);

CREATE TABLE IF NOT EXISTS public.price
(
  id    SERIAL PRIMARY KEY,
  price DECIMAL DEFAULT 0
);

CREATE SEQUENCE public.product_serial_id
  START WITH 1000
  INCREMENT BY 1
  NO MINVALUE
  NO MAXVALUE;

CREATE TABLE IF NOT EXISTS public.product
(
  id               INTEGER DEFAULT nextval('public.product_serial_id'::regclass) NOT NULL PRIMARY KEY,
  sku              VARCHAR(255)                                                  NOT NULL,
  name             VARCHAR(255)                                                  NOT NULL,
  description      TEXT,
  brand_id         INTEGER
    CONSTRAINT product_brand_id_fkey REFERENCES public.brand,
  price_id         INTEGER
    constraint product_price_id REFERENCES public.price,
  quantity         INTEGER,
  taxable          BOOLEAN,
  created_at       TIMESTAMP WITH TIME ZONE                                      NOT NULL,
  updated_at       TIMESTAMP WITH TIME ZONE
);

-- migrate:down
DROP TABLE IF EXISTS public.product;
DROP TABLE IF EXISTS public.price;
DROP TABLE IF EXISTS public.brand;

DROP SEQUENCE IF EXISTS public.product_serial_id;

