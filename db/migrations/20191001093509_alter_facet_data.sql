-- migrate:up
ALTER TABLE public.facet_data
  ADD COLUMN data_type TEXT;
-- migrate:down

ALTER TABLE public.facet_data
  DROP COLUMN IF EXISTS data_type