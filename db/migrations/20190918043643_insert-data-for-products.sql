-- migrate:up
-- insert data to product table
insert into product (id, sku, name, dep, path, created_at) values (1, 'MD001', 'product 1', 1, '1', 'now()');
insert into product (id, sku, name, dep, path, created_at) values (2, 'MD002', 'product 2', 2, '2', 'now()');
insert into product (id, sku, name, dep, path, created_at) values (3, 'MD003', 'product 3', 3, '3', 'now()');
insert into product (id, sku, name, dep, path, created_at) values (4, 'MD004', 'product 4', 4, '4', 'now()');
insert into product (id, sku, name, dep, path, created_at) values (5, 'MD005', 'product 5', 5, '5', 'now()');
insert into product (id, sku, name, dep, path, created_at) values (6, 'MD006', 'product 6', 6, '6', 'now()');
insert into product (id, sku, name, dep, path, created_at) values (7, 'MD007', 'product 7', 7, '7', 'now()');
insert into product (id, sku, name, dep, path, created_at) values (8,'MD008', 'product 8', 8, '8', 'now()');
insert into product (id, sku, name, dep, path, created_at) values (9, 'MD009', 'product 9', 9, '9', 'now()');
insert into product (id, sku, name, dep, path, created_at) values (10, 'MD0010', 'product 10', 10, '10', 'now()');
insert into product (id, sku, name, dep, path, created_at) values (11, 'MD0011', 'product 11', 11, '11', 'now()');
insert into product (id, sku, name, dep, path, created_at) values (12, 'MD0012', 'product 12', 12, '12', 'now()');
insert into product (id, sku, name, dep, path, created_at) values (13, 'MD0013', 'product 13', 2, '2/13', 'now()');
insert into product (id, sku, name, dep, path, created_at) values (14, 'MD0014', 'product 14', 2, '2/13/14', 'now()');
insert into product (id, sku, name, dep, path, created_at) values (15, 'MD0015', 'product 15', 2, '2/13/15', 'now()');
insert into product (id, sku, name, dep, path, created_at) values (16, 'MD0016', 'product 15', 2, '2/13/14/16', 'now()');
insert into product (id, sku, name, dep, path, created_at) values (17, 'MD0017', 'product 16', 2, '2/13/14/17', 'now()');
insert into product (id, sku, name, dep, path, created_at) values (18, 'MD0018', 'product 17', 2, '2/13/14/18', 'now()');
insert into product (id, sku, name, dep, path, created_at) values (19, 'MD0019', 'product 18', 2, '2/13/14/19', 'now()');
insert into product (id, sku, name, dep, path, created_at) values (20, 'MD0020', 'product 20', 2, '2/13/14/20', 'now()');

-- insert data to category_product table
INSERT INTO public.category_product (category_id, product_id) VALUES (1, 1);
INSERT INTO public.category_product (category_id, product_id) VALUES (2, 2);
INSERT INTO public.category_product (category_id, product_id) VALUES (3, 3);
INSERT INTO public.category_product (category_id, product_id) VALUES (4, 4);
INSERT INTO public.category_product (category_id, product_id) VALUES (5, 5);
INSERT INTO public.category_product (category_id, product_id) VALUES (6, 6);
INSERT INTO public.category_product (category_id, product_id) VALUES (7, 7);
INSERT INTO public.category_product (category_id, product_id) VALUES (8, 8);
INSERT INTO public.category_product (category_id, product_id) VALUES (9, 9);
INSERT INTO public.category_product (category_id, product_id) VALUES (10, 10);
INSERT INTO public.category_product (category_id, product_id) VALUES (11, 11);
INSERT INTO public.category_product (category_id, product_id) VALUES (12, 12);
INSERT INTO public.category_product (category_id, product_id) VALUES (13, 13);
INSERT INTO public.category_product (category_id, product_id) VALUES (14, 14);
INSERT INTO public.category_product (category_id, product_id) VALUES (15, 15);
INSERT INTO public.category_product (category_id, product_id) VALUES (16, 16);
INSERT INTO public.category_product (category_id, product_id) VALUES (17, 17);
INSERT INTO public.category_product (category_id, product_id) VALUES (18, 18);
INSERT INTO public.category_product (category_id, product_id) VALUES (19, 19);
INSERT INTO public.category_product (category_id, product_id) VALUES (20, 20);

-- migrate:down
DELETE FROM public.category_product WHERE category_product.category_id >= 1 AND category_product.category_id <= 20;
DELETE FROM public.product WHERE product.id >= 1 AND product.id <= 20;
ALTER SEQUENCE product_serial_id RESTART WITH 1;
