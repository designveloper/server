-- migrate:up
ALTER TABLE product
  ADD COLUMN dep INTEGER,
  ADD COLUMN path VARCHAR;

-- migrate:down
ALTER TABLE product
  DROP COLUMN IF EXISTS path,
  DROP COLUMN IF EXISTS dep;


