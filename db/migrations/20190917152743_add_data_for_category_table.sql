-- migrate:up
-- INSERT Categories level 1
INSERT INTO public.category(id, name, parent_id, created_at) VALUES (1, 'Agriculture & Food', NULL, NOW());
INSERT INTO public.category(id, name, parent_id, created_at) VALUES (2, 'Apparel,Textiles & Accessories', NULL, NOW());
INSERT INTO public.category(id, name, parent_id, created_at) VALUES (3, 'Auto & Transportation', NULL, NOW());
INSERT INTO public.category(id, name, parent_id, created_at) VALUES (4, 'Bags, Shoes & Accessories', NULL, NOW());
INSERT INTO public.category(id, name, parent_id, created_at) VALUES (5, 'Electronics', NULL, NOW());
INSERT INTO public.category(id, name, parent_id, created_at) VALUES (6, 'Electrical Equipment, Components & Telecoms', NULL, NOW());
INSERT INTO public.category(id, name, parent_id, created_at) VALUES (7, 'Gifts, Sports & Toys', NULL, NOW());
INSERT INTO public.category(id, name, parent_id, created_at) VALUES (8, 'Health & Beauty', NULL, NOW());
INSERT INTO public.category(id, name, parent_id, created_at) VALUES (9, 'Home, Lights & Construction', NULL, NOW());
INSERT INTO public.category(id, name, parent_id, created_at) VALUES (10, 'Machinery, Industrial Parts & Tools', NULL, NOW());
INSERT INTO public.category(id, name, parent_id, created_at) VALUES (11, 'Metal, Chemicals, Rubber & Plastics', NULL, NOW());
INSERT INTO public.category(id, name, parent_id, created_at) VALUES (12, 'Packaging, Advertising & Office', NULL, NOW());

-- INSERT categories level 2
INSERT INTO public.category(id, name, parent_id, created_at) VALUES (13, 'Apparel', 2, NOW());

-- INSERT categories level 3
INSERT INTO public.category(id, name, parent_id, created_at) VALUES (14, 'Sportswear', 13, NOW());

-- INSERT categories level 4
INSERT INTO public.category(id, name, parent_id, created_at) VALUES (15, 'American Football Wear', 14, NOW());
INSERT INTO public.category(id, name, parent_id, created_at) VALUES (16, 'Baseball & Softball Wear', 14, NOW());
INSERT INTO public.category(id, name, parent_id, created_at) VALUES (17, 'Basketball Wear', 14, NOW());
INSERT INTO public.category(id, name, parent_id, created_at) VALUES (18, 'Camping & Hiking Wear', 14, NOW());
INSERT INTO public.category(id, name, parent_id, created_at) VALUES (19,'Cycling Wear', 14, NOW());
INSERT INTO public.category(id, name, parent_id, created_at) VALUES (20, 'Fishing Wear', 14, NOW());
INSERT INTO public.category(id, name, parent_id, created_at) VALUES (21, 'Fitness & Yoga Wear', 14, NOW());
INSERT INTO public.category(id, name, parent_id, created_at) VALUES (22, 'Ice Hockey Wear', 14, NOW());
INSERT INTO public.category(id, name, parent_id, created_at) VALUES (23, 'Martial Arts Wear', 14, NOW());
INSERT INTO public.category(id, name, parent_id, created_at) VALUES (24, 'Motorcycle & Auto Racing Wear', 14, NOW());
INSERT INTO public.category(id, name, parent_id, created_at) VALUES (25, 'Other Sportswear', 14, NOW());
INSERT INTO public.category(id, name, parent_id, created_at) VALUES (26, 'Rash Guard', 14, NOW());
INSERT INTO public.category(id, name, parent_id, created_at) VALUES (27, 'Rugby Football Wear', 14, NOW());
INSERT INTO public.category(id, name, parent_id, created_at) VALUES (28, 'Running Wear', 14, NOW());
INSERT INTO public.category(id, name, parent_id, created_at) VALUES (29, 'Ski & Snow Wear', 14, NOW());
INSERT INTO public.category(id, name, parent_id, created_at) VALUES (30, 'Soccer Wear', 14, NOW());
INSERT INTO public.category(id, name, parent_id, created_at) VALUES (31, 'Swimwear & Beachwear', 14, NOW());
INSERT INTO public.category(id, name, parent_id, created_at) VALUES (32, 'Tennis Wear', 14, NOW());
INSERT INTO public.category(id, name, parent_id, created_at) VALUES (33, 'Training & Jogging Wear', 14, NOW());
INSERT INTO public.category(id, name, parent_id, created_at) VALUES (34, 'Wetsuits', 14, NOW());

-- migrate:down
DELETE FROM public.category WHERE category.id >= 1 AND category.id <= 34;
ALTER SEQUENCE category_serial_id RESTART WITH 1;
