-- migrate:up
CREATE TABLE IF NOT EXISTS public.facet_set
(
  category_id INTEGER NOT NULL
    CONSTRAINT facet_set_category_id_fkey REFERENCES public.category,
  facet_id    INTEGER NOT NULL
    CONSTRAINT facet_set_facet_id_fkey REFERENCES public.facet,
  UNIQUE (category_id, facet_id)
);

-- migrate:down

