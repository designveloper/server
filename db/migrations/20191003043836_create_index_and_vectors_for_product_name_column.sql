-- migrate:up
ALTER TABLE public.product
  ADD COLUMN name_vectors tsvector;

CREATE INDEX product_name_vectors_idx ON product using GIN (name_vectors);

UPDATE public.product
SET name_vectors = (to_tsvector(name) || to_tsvector(REGEXP_REPLACE(name, '\s+', '', 'g')));

-- migrate:down
ALTER TABLE public.product
  DROP COLUMN IF EXISTS name_vectors;

DROP INDEX IF EXISTS product_name_vectors_idx;

