-- migrate:up
ALTER TABLE public.category
  ADD COLUMN name_vectors tsvector;

CREATE INDEX category_name_vectors_idx ON category using GIN (name_vectors);

UPDATE public.category
SET name_vectors = (to_tsvector(name) || to_tsvector(REGEXP_REPLACE(name, '\s+', '', 'g')));
-- migrate:down
ALTER TABLE public.category
  DROP COLUMN IF EXISTS name_vectors;

DROP INDEX IF EXISTS category_name_vectors_idx;

