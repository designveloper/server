-- migrate:up
ALTER SEQUENCE category_serial_id
  RESTART WITH 1;
ALTER SEQUENCE product_serial_id
  RESTART WITH 1;
-- migrate:down

