-- migrate:up
CREATE TABLE IF NOT EXISTS public.role
(
  id         SMALLSERIAL PRIMARY KEY,
  name       VARCHAR(20) NOT NULL,
  created_at TIMESTAMP WITH TIME ZONE,
  updated_at TIMESTAMP WITH TIME ZONE
);

CREATE TABLE IF NOT EXISTS public.user
(
  id              uuid PRIMARY KEY,
  email           VARCHAR(255)             NOT NULL,
  UNIQUE (email),
  first_name      VARCHAR(255)             NOT NULL,
  last_name       VARCHAR(255)             NOT NULL,
  password        VARCHAR(60)              NOT NULL,
  created_at      TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW(),
  activated_at    TIMESTAMP WITH TIME ZONE,
  updated_at      TIMESTAMP WITH TIME ZONE,
  last_connection TIMESTAMP WITH TIME ZONE,
  activation_code VARCHAR(10)
);

CREATE TABLE IF NOT EXISTS public.user_role
(
  user_id uuid
    CONSTRAINT user_role_user_id_fkey REFERENCES public.user,
  role_id INTEGER
    CONSTRAINT user_role_role_id_fkey REFERENCES public.role,
  UNIQUE (user_id, role_id)
);

-- migrate:down
DROP TABLE IF EXISTS public.user_role;
DROP TABLE IF EXISTS public.user;
DROP TABLE IF EXISTS public.role;
