package config

import (
	"flag"
	"fmt"
	"reflect"
	"strings"
	"time"

	validation "github.com/go-ozzo/ozzo-validation"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
)

// Config stores the application-wide configurations
var (
	Config       appConfig
	IsProduction = flag.Bool("production", false, "run in PRODUCTION mode")
)

// AppConfig structure for configuration
type appConfig struct {
	// The secret used within the app
	MekongSecret string `mapstructure:"mekong_secret"`
	// the server port. Defaults to 8080
	ServerPort int `mapstructure:"server_port"`
	// the database connection string
	DatabaseURL string `mapstructure:"database_url"`
	// The data necessary to send mail
	SMTPMail string `mapstructure:"smtp_mail"`
	// The data necessary to send mail
	SMTPPort int `mapstructure:"smtp_port"`
	// The data necessary to send mail
	SMTPPassword string `mapstructure:"smtp_password"`
	// The data necessary to send mail
	SMTPServer string `mapstructure:"smtp_server"`
	// The jwt signed secret
	JwtSignedSecret string `mapstructure:"jwt_signed_secret"`
	// The url of webapp
	MekongWebappHost string `mapstructure:"mekong_webapp_host"`
	// The timeout of reading progress
	ServerReadTimeout time.Duration `mapstructure:"server_read_timeout"`
	// The timeout of writing progress
	ServerWriteTimeout time.Duration `mapstructure:"server_write_timeout"`
}

func init() {
	flag.Parse()
}

// Validate check if the configuation is valid or not
func (config appConfig) Validate() error {
	return validation.ValidateStruct(&config,
		validation.Field(&config.ServerPort, validation.Required),
		validation.Field(&config.DatabaseURL, validation.Required),
	)
}

// LoadConfig loads configuration from the given list of paths and populates it into the Config variable.
// Run without flag -production: Run in DEV mode, load config from `config/app.yaml`
// Run with flag -production: Run in PRODUCTION mode, load config from env vars only
func LoadConfig(configPaths ...string) error {
	v := viper.New()

	v.SetDefault("mekong_secret", "dont share mekong depot secret with anyone")
	v.SetDefault("server_port", 8080)
	v.SetDefault("jwt_signed_secret", "mekong depot")
	v.SetDefault("mekong_webapp_host", "http://localhost:3000")
	v.SetDefault("server_read_timeout", 2*time.Minute)
	v.SetDefault("server_write_timeout", 2*time.Minute)

	if *IsProduction {
		logrus.Infof("Load configuration in PRODUCTION mode")
		logrus.Infof("Make sure all needed configurations are set in environment")

		v.SetDefault("mekong_webapp_host", "http://staging.mekongdepot.com")
		v.SetDefault("server_read_timeout", 10*time.Second)
		v.SetDefault("server_write_timeout", 10*time.Second)
		v.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))
		// Read the variables which start with "MD_".
		v.SetEnvPrefix("md")

		_ = v.ReadInConfig()

		BindEnvs(Config, v)
	} else {
		logrus.Infof("Load configuration in DEV mode")
		v.SetConfigName("app")
		v.SetConfigType("yaml")
		v.AutomaticEnv()
		for _, path := range configPaths {
			v.AddConfigPath(path)
		}
		if err := v.ReadInConfig(); err != nil {
			return fmt.Errorf("failed to read the configuration file: %s", err)
		}
	}

	if err := v.Unmarshal(&Config); err != nil {
		return err
	}

	return Config.Validate()
}

// BindEnvs use to bind env vars
func BindEnvs(iface interface{}, vp *viper.Viper, parts ...string) {
	ifv := reflect.ValueOf(iface)
	ift := reflect.TypeOf(iface)
	for i := 0; i < ift.NumField(); i++ {
		v := ifv.Field(i)
		t := ift.Field(i)
		tv, ok := t.Tag.Lookup("mapstructure")
		if !ok {
			continue
		}
		switch v.Kind() {
		case reflect.Struct:
			BindEnvs(v.Interface(), vp, append(parts, tv)...)
		default:
			_ = vp.BindEnv(strings.Join(append(parts, tv), "."))
		}
	}
}
