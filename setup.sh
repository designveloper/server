#!/bin/bash

# We don't need to install them separately since these all are included in gometalinter by default
# echo "> Updating goimports"
# go get -u golang.org/x/tools/cmd/goimports
#
# echo "> Updating golint"
# go get -u golang.org/x/lint/golint

# echo "> Installing & updating metalinter. Read https://github.com/alecthomas/gometalinter for more information."
# curl -L https://git.io/vp6lP | sh
# gometalinter --install

echo "> Installing & updating golangci-lint. Read https://github.com/golangci/golangci-lint for more information."
go get -u github.com/golangci/golangci-lint/cmd/golangci-lint

echo "> Updating hooks"
cp .hooks/* .git/hooks/
