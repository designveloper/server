package app

import (
	"time"

	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
)

// Init returns a middleware that prepares the request context and processing environment.
func Init(logger *logrus.Logger) gin.HandlerFunc {
	return func(c *gin.Context) {
		now := time.Now()

		ac := newRequestScope(now, logger, c.Request, c)
		c.Set("Context", ac)
		c.Next()
	}
}

// GetRequestScope returns the RequestScope of the current request.
func GetRequestScope(c *gin.Context) RequestScope {
	requestScope, _ := c.Get("Context")
	//if !ok {
	//	return nil, errors.New("Can not get request context")
	//}

	return requestScope.(RequestScope)
}
