package errors

import (
	"github.com/gin-gonic/gin"

	errs "errors"

	httpRes "bitbucket.org/designveloper/mekong-api/core/http_response"
	"github.com/lib/pq"
)

// Wrap wrap ErrorContainer
func Wrap(c *gin.Context, errContainer httpRes.ErrorContainer) {
	_ = c.Error(errs.New("empty")).SetMeta(errContainer)
}

// InternalServerError returns an wrapped ErrorContainer from given business error
// If input error is a self ErrorContainer, then return it because it's business error defined in UED
// Else wrap it as ErrorContainer with desired error
func InternalServerError(err interface{}, msg string) httpRes.ErrorContainer {
	if _, ok := err.(httpRes.ErrorContainer); ok {
		return err.(httpRes.ErrorContainer)
	}

	if _, ok := err.(error); !ok {
		return httpRes.UEDMap[httpRes.ErrorInternalServerError].
			SetNilDebugTrace().
			SetDebugMessage(msg)
	}

	return httpRes.UEDMap[httpRes.ErrorInternalServerError].
		SetDebugTrace(err.(error)).
		SetDebugMessage(msg)
}

// SuccessRequest returns an wrapped ErrorContainer from given success request
func SuccessRequest() httpRes.ErrorContainer {
	return httpRes.UEDMap[httpRes.SuccessRequest].
		SetDebugTrace(nil).
		SetDebugMessage("")
}

// BadRequest returns an wrapped ErrorContainer from given bad request error
func BadRequest(err error, msg string) httpRes.ErrorContainer {
	if _, ok := err.(httpRes.ErrorContainer); ok {
		return err.(httpRes.ErrorContainer)
	}

	if _, ok := err.(error); !ok {
		return httpRes.UEDMap[httpRes.ErrorBadRequest].
			SetNilDebugTrace().
			SetDebugMessage(msg)
	}

	return httpRes.UEDMap[httpRes.ErrorBadRequest].
		SetDebugTrace(err.(error)).
		SetDebugMessage(msg)
}

// Unauthorized returns an wrapped ErrorContainer from given unauthorized error
func Unauthorized(err error, msg string) httpRes.ErrorContainer {
	if _, ok := err.(httpRes.ErrorContainer); ok {
		return err.(httpRes.ErrorContainer)
	}

	if _, ok := err.(error); !ok {
		return httpRes.UEDMap[httpRes.ErrorInternalServerError].
			SetNilDebugTrace().
			SetDebugMessage(msg)
	}
	return httpRes.UEDMap[httpRes.ErrorUnauthorized].
		SetDebugTrace(err).
		SetDebugMessage(msg)
}

// AccessForbidden returns an wrapped ErrorContainer from given access forbidden error
func AccessForbidden(err error, msg string) httpRes.ErrorContainer {
	return httpRes.UEDMap[httpRes.ErrorAccessForbidden].
		SetDebugTrace(err).
		SetDebugMessage(msg)
}

// ResourceNotFound returns an wrapped ErrorContainer from given resource not found error
func ResourceNotFound(err error, msg string) httpRes.ErrorContainer {
	return httpRes.UEDMap[httpRes.ErrorResourceNotFound].
		SetDebugTrace(err).
		SetDebugMessage(msg)
}

// PaymentFailedGetCheckout returns an wrapped ErrorContainer from given payment failed get checkout error
func PaymentFailedGetCheckout(err error, msg string) httpRes.ErrorContainer {
	return httpRes.UEDMap[httpRes.ErrorPaymentFailedGetCheckout].
		SetDebugTrace(err).
		SetDebugMessage(msg)
}

// PaymentFailedGetResource returns an wrapped ErrorContainer from given payment failed get resource error
func PaymentFailedGetResource(err error, msg string) httpRes.ErrorContainer {
	return httpRes.UEDMap[httpRes.ErrorPaymentFailedGetResource].
		SetDebugTrace(err).
		SetDebugMessage(msg)
}

// PaymentFailedRegisterPremium returns an wrapped ErrorContainer from given payment failed register premium error
func PaymentFailedRegisterPremium(err error, msg string) httpRes.ErrorContainer {
	return httpRes.UEDMap[httpRes.ErrorPaymentFailedRegisterPremium].
		SetDebugTrace(err).
		SetDebugMessage(msg)
}

// PaymentFailedCheckPremium returns an wrapped ErrorContainer from given payment failed check premium error
func PaymentFailedCheckPremium(err error, msg string) httpRes.ErrorContainer {
	return httpRes.UEDMap[httpRes.ErrorPaymentFailedCheckPremium].
		SetDebugTrace(err).
		SetDebugMessage(msg)
}

// OTPEmptyCode returns an wrapped ErrorContainer from given OTP empty code
func OTPEmptyCode() httpRes.ErrorContainer {
	return httpRes.UEDMap[httpRes.ErrorOTPEmptyCode]
}

// OTPExpiredCode returns an wrapped ErrorContainer from given OTP empty code
func OTPExpiredCode() httpRes.ErrorContainer {
	return httpRes.UEDMap[httpRes.ErrorOTPExpiredCode]
}

// InvalidAccessToken returns an wrapped ErrorContainer from given access token error
func InvalidAccessToken(err error, msg string) httpRes.ErrorContainer {
	return httpRes.UEDMap[httpRes.ErrorInvalidAccessToken].
		SetDebugTrace(err).
		SetDebugMessage(msg)
}

// ErrorVerify  parse error and return valididity
func ErrorVerify(err error) (bool, error) {
	if err, ok := err.(*pq.Error); ok {
		if err.Code.Class().Name() == "data_exception" {
			return true, BadRequest(err, err.Error())
		}
	}
	return false, err
}

// ParseSQLError correct error
func ParseSQLError(err error) error {
	if err == nil {
		return err
	}

	pqErr, ok := err.(*pq.Error)
	if !ok {
		return InternalServerError(err, "")
	}

	switch pqErr.Code {
	case "23505":
		return httpRes.UEDMap[httpRes.ErrorUniqueViolation]
	}

	return err
}

// AuthUnableToFetchToken returns an wrapped ErrorContainer from perm that unable to fetch token
func AuthUnableToFetchToken(err error, msg string) httpRes.ErrorContainer {
	return httpRes.UEDMap[httpRes.ErrorAuthUnableToFetchToken].
		SetDebugTrace(err).
		SetDebugMessage(msg)
}

// UnmatchedData returns unmatched data error
func UnmatchedData(err error, msg string) httpRes.ErrorContainer {
	return httpRes.UEDMap[httpRes.ErrorUnmatchedData].
		SetDebugTrace(err).
		SetDebugMessage(msg)
}

// AuthAccountNotActive returns account not activated error
func AuthAccountNotActive(err error, msg string) httpRes.ErrorContainer {
	return httpRes.UEDMap[httpRes.ErrorAccountNotActive].
		SetDebugTrace(err).
		SetDebugMessage(msg)
}

// DuplicateRegisteredEmail returns duplicate email error
func DuplicateRegisteredEmail(err error, msg string) httpRes.ErrorContainer {
	if _, ok := err.(httpRes.ErrorContainer); ok {
		return err.(httpRes.ErrorContainer)
	}

	if _, ok := err.(error); !ok {
		return httpRes.UEDMap[httpRes.ErrorInternalServerError].
			SetNilDebugTrace().
			SetDebugMessage(msg)
	}

	return httpRes.UEDMap[httpRes.ErrorDuplicateRegisteredEmail].
		SetDebugTrace(err.(error)).
		SetDebugMessage(msg)
}
