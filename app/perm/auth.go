package perm

import (
	"encoding/json"
	"errors"
	"time"

	"bitbucket.org/designveloper/mekong-api/app"
	"bitbucket.org/designveloper/mekong-api/app/domain/model"
	appErrors "bitbucket.org/designveloper/mekong-api/app/errors"
	appConfig "bitbucket.org/designveloper/mekong-api/config"

	jwtGo "github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
)

const (
	permDebugMessage = "permDebugMessage"
)

// MapClaims a claim map
type MapClaims map[string]interface{}

// IsAuthenticate must have this permission to access the group request
func IsAuthenticate(r *gin.RouterGroup) *gin.RouterGroup {
	rg := r.Group("")
	// Use middleware to check if token is valid
	rg.Use(JWTHandler())
	return rg
}

// ExtractClaims help to extract the JWT claims
func ExtractClaims(c *gin.Context) jwtGo.MapClaims {
	claims, exists := c.Get("JWT_PAYLOAD")
	if !exists {
		return make(jwtGo.MapClaims)
	}

	return claims.(jwtGo.MapClaims)
}

// JWTHandler decrypt token to find identity of user
func JWTHandler() gin.HandlerFunc {
	return func(c *gin.Context) {
		//Decode JWT TOKEN
		claims := ExtractClaims(c)
		jsonEncodeIdentity, ok := claims["identity"].(string)
		if !ok {
			_ = c.Error(errors.New("empty")).SetMeta(appErrors.AuthUnableToFetchToken(nil, "123"))
			return
		}

		var userIdentity model.UserIdentity
		if err := json.Unmarshal([]byte(jsonEncodeIdentity), &userIdentity); err != nil {
			_ = c.Error(errors.New("empty")).SetMeta(appErrors.AuthUnableToFetchToken(err, "321"))
			return
		}

		app.GetRequestScope(c).SetIdentity(userIdentity)
		c.Next()
	}
}

// CreateToken generate token based on user identity
func CreateToken(identity model.Identity) (string, error) {
	jsonIdentity, err := json.Marshal(identity)
	if err != nil {
		return "", appErrors.InvalidAccessToken(err, permDebugMessage)
	}

	token := jwtGo.NewWithClaims(jwtGo.SigningMethodHS256, jwtGo.MapClaims{
		"identity": string(jsonIdentity),
		"exp":      time.Now().Add(time.Hour * 10).Unix(),
	})

	tokenString, err := token.SignedString([]byte(appConfig.Config.JwtSignedSecret))
	if err != nil {
		return "", appErrors.InvalidAccessToken(err, permDebugMessage)
	}

	return tokenString, nil
}
