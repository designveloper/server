package util

import (
	"crypto/rand"
	"crypto/sha256"
	"encoding/base64"
	"fmt"
	mr "math/rand"
	"strings"
	"time"

	"golang.org/x/crypto/bcrypt"
)

// GenerateRandomBytes returns securely generated random bytes.
// It will return an error if the system's secure random
// number generator fails to function correctly, in which
// case the caller should not continue.
func GenerateRandomBytes(n int) ([]byte, error) {
	b := make([]byte, n)
	_, err := rand.Read(b)
	// Note that err == nil only if we read len(b) bytes.
	if err != nil {
		return nil, err
	}

	return b, nil
}

// GenerateRandomPassNumber return number with 9 digit
func GenerateRandomStrings(length int) (string, error) {
	token, err := GenerateRandomBytes(length / 2)
	if err != nil {
		return "", err
	}
	return fmt.Sprintf("%x", token), nil
}

// GenerateRandomPassPhone return number to link Phone
func GenerateRandomPassPhone() int {
	mr.Seed(time.Now().UnixNano())
	return RandomInt(100000, 999999)
}

// GenerateRandomPassNumber return number with 9 digit
func GenerateRandomPassNumber(length int) (string, error) {
	token, err := GenerateRandomBytes(length / 2)
	if err != nil {
		return "", err
	}
	return strings.ToUpper(fmt.Sprintf("%x", token)), nil
}

// RandomInt Returns an int >= min, < max
func RandomInt(min, max int) int {
	return min + mr.Intn(max-min)
}

// Token returns a URL-safe, base64 encoded
// securely generated random string.
// It will return an error if the system's secure random
// number generator fails to function correctly, in which
// case the caller should not continue.
func Token(size int) (string, error) {
	b, err := GenerateRandomBytes(size)
	return base64.URLEncoding.EncodeToString(b), err
}

func init() {
	mr.Seed(time.Now().UnixNano())
}

var letterRunes = []rune("0123456789")

// RandomNumberString generate random string with int only
func RandomNumberString(n int) string {
	b := make([]rune, n)
	for i := range b {
		b[i] = letterRunes[mr.Intn(len(letterRunes))]
	}
	return string(b)
}

// Hash return hash password
func Hash(password string) (string, error) {
	h, err := bcrypt.GenerateFromPassword([]byte(password), 12) //goal: 16
	return string(h), err
}

// HashWithCost return hash password with given cost
func HashWithCost(password string, cost int) (string, error) {
	h, err := bcrypt.GenerateFromPassword([]byte(password), cost)
	return string(h), err
}

// HashSHA256 return hash password
func HashSHA256(password string) string {
	hasher := sha256.New()
	_, _ = hasher.Write([]byte(password))
	return base64.URLEncoding.EncodeToString(hasher.Sum(nil))
}

// ComparePasswordMatch return and error if match is't invalid
func ComparePasswordMatch(password, hash string) error {
	return bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
}

var validRoles = []string{"professional", "patient", "entity-admin", "pnm"}
var validRolesStaff = []string{"moderator", "superadmin"}

// ValidRole return true if role is valide
func ValidRole(role string) bool {
	for _, r := range validRoles {
		if r == role {
			return true
		}
	}
	return false
}

// ValidRoleStaff return true if role is valid
func ValidRoleStaff(role string) bool {
	for _, r := range validRolesStaff {
		if r == role {
			return true
		}
	}
	return false
}

// EncodeEmail for access request
func EncodeEmail(email string) string {
	encodeEmail := fmt.Sprintf("%s*****%s", email[:2], email[strings.Index(email, "@"):])

	return encodeEmail
}

// EncodePhone for access request
func EncodePhone(phone string) string {
	encodePhone := fmt.Sprintf("%s****", phone[:len(phone)-4])

	return encodePhone
}
