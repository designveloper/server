package util

import (
	dbx "github.com/go-ozzo/ozzo-dbx"

	"bitbucket.org/designveloper/mekong-api/app/domain/model"
)

// BuildUserFilter build user filter
func BuildUserFilter(f ...model.UserFilter) dbx.Expression {
	b := dbx.HashExp{}
	for _, val := range f {
		b[val.Key] = val.Value
	}
	return b
}
