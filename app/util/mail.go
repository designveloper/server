package util

import (
	"bytes"
	"errors"
	"fmt"
	"html/template"
	"net/smtp"

	log "github.com/sirupsen/logrus"

	"bitbucket.org/designveloper/mekong-api/app/middleware"
	appConfig "bitbucket.org/designveloper/mekong-api/config"
)

var (
	mime = "MIME-version: 1.0;\nContent-Type: text/html; charset=\"UTF-8\";\n\n" //renommer en MIMETypeHTML

	email          string
	password       string
	port           int
	server         string
	smtpServerAddr string
	smtpAuth       smtp.Auth
)

const (
	ENG_LANG = "ENGLISH"
	VIE_LANG = "VIETNAM"
)

// SetSMTPConfig is called to set package global variables at init
func SetSMTPConfig(configPort int, configEmail, configPassword, configServer string) error {
	switch {
	case configPort == 0:
		return errors.New("init: smtp: missing configPort")
	case configEmail == "":
		return errors.New("init: smtp: missing configEmail")
	case configServer == "":
		return errors.New("init: smtp: missing configServer")
	case configPassword == "":
		return errors.New("init: smtp: missing configPassword")
	}

	email = configEmail
	port = configPort
	server = configServer
	password = configPassword
	smtpServerAddr = fmt.Sprintf("%s:%d", server, port)
	smtpAuth = smtp.PlainAuth("", email, password, server)

	log.Infof("init: smtp: configuration loaded successfully")

	return nil
}

// Config represent data necessary to send a mail
type Config struct {
	Server   string
	Port     int
	Email    string
	Password string
}

// Mail mmodel with user mail, subject mail and body mail
type Mail struct {
	to      []string
	replyTo string
	subject string
	body    string
}

// newMail return mail type
func newMail(to []string, subject string) *Mail {
	return &Mail{
		to:      to,
		subject: subject,
	}
}

// parseTemplate take template and interface parameter to return body mail
func (r *Mail) parseTemplate(fileName string, data interface{}) error {
	t, err := template.ParseFiles(fileName)
	if err != nil {
		return err
	}

	buffer := new(bytes.Buffer)
	if err = t.Execute(buffer, data); err != nil {
		return err
	}

	r.body = buffer.String()
	return nil
}

// sendMail send a mail
func (r *Mail) sendMail() error {
	if len(r.to) < 1 {
		return errors.New("mail 'to' property is empty")
	}
	body := fmt.Sprintf("To: %s\r\nSubject: %s\r\n%s\r\n%s", r.to[0], r.subject, mime, r.body)

	if r.replyTo != "" {
		body = fmt.Sprintf("Reply-To: %s\r\n%s", r.replyTo, body)
	}

	return smtp.SendMail(smtpServerAddr, smtpAuth, email, []string{r.to[0]}, []byte(body))
}

// Send Create template with items and template string got with filename parameter and send mail
func (r *Mail) Send(templateName string, items interface{}) error {
	if err := r.parseTemplate(templateName, items); err != nil {
		return err
	}

	return r.sendMail()
}

// SendRegisterEmail take mail name and login parameter
func SendRegisterEmail(mail, language, activationCode string) error {
	webAppActivationLink := fmt.Sprintf("%s/user/activate?email=%s&activation_code=%s", appConfig.Config.MekongWebappHost, mail, activationCode)
	supportUrl := fmt.Sprintf("%s/support", appConfig.Config.MekongWebappHost)
	templateData := struct {
		URL        string
		SupportUrl string
	}{
		webAppActivationLink,
		supportUrl,
	}

	var templateFilePath, subject string

	switch language {
	case middleware.VieLang:
		{
			templateFilePath = "templates/register_verify_vi.html"
			subject = "Chào mừng bạn đến với Mekong Depot"
		}
	default:
		{
			templateFilePath = "templates/register_verify_en.html"
			subject = "Welcome to Mekong Depot"
		}
	}
	r := newMail([]string{mail}, subject)

	return r.Send(templateFilePath, templateData)
}

// SendResetPasswordEmail take mail name and send to user link to reset password
func SendResetPasswordEmail(mail, language, token string) error {
	webAppActivationLink := fmt.Sprintf("%s/user/reset-password?token=%s", appConfig.Config.MekongWebappHost, token)
	templateData := struct {
		URL string
	}{
		webAppActivationLink,
	}

	var templateFilePath, subject string

	switch language {
	case middleware.VieLang:
		{
			templateFilePath = "templates/reset_password_vi.html"
			subject = "Thay đổi mật khẩu"
		}
	default:
		{
			templateFilePath = "templates/reset_password_en.html"
			subject = "Reset password"
		}
	}
	r := newMail([]string{mail}, subject)

	return r.Send(templateFilePath, templateData)
}

// SendResetPasswordEmailSuccess take mail name and send to user link to reset password successful
func SendResetPasswordEmailSuccess(mail, name, language string) error {
	templateData := struct {
		Name string
	}{
		name,
	}

	var templateFilePath, subject string

	switch language {
	case middleware.VieLang:
		{
			templateFilePath = "templates/reset_password_success_vi.html"
			subject = "Thay đổi mật khẩu thành công"
		}
	default:
		{
			templateFilePath = "templates/reset_password_success_en.html"
			subject = "Reset password successful"
		}
	}
	r := newMail([]string{mail}, subject)

	return r.Send(templateFilePath, templateData)
}
