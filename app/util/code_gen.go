package util

import "bitbucket.org/designveloper/mekong-api/core/util"

const accessCodeCharset = "ABCDEFGHIJKLMNPQRSTUVWXYZ123456789"

// RandActivateCode generate random access code
func RandActivateCode() string {
	return util.RandStringWithCharset(8, accessCodeCharset)
}

// RandResetPasswordToken generate random reset password token
func RandResetPasswordToken() string {
	return util.RandStringWithCharset(12, accessCodeCharset)
}
