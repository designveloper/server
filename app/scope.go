package app

import (
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	dbx "github.com/go-ozzo/ozzo-dbx"
	"github.com/sirupsen/logrus"

	"bitbucket.org/designveloper/mekong-api/app/domain/model"
)

// RequestScope contains the application-specific information that are carried around in a request.
type RequestScope interface {
	// RequestID returns the ID of the current request
	RequestID() string
	// SetDB sets the database connection
	SetDB(db *dbx.DB)
	// Executor is the db handler used in daos function, implement all methods of dbx.Builder
	Executor() dbx.Builder
	// SetExecutor casts DB/Tx as an executor
	SetExcutor(interface{})
	// Rollback returns a value indicating whether the current database transaction should be rolled back
	Rollback() bool
	// SetRollback sets a value indicating whether the current database transaction should be rolled back
	SetRollback(bool)
	// Now returns the timestamp representing the time when the request is being processed
	Now() time.Time
	// SetUser sets the user of the current transaction
	SetIdentity(user model.Identity)
	// SetShouldTokenRefresh set true if we need to refresh AuthToken with new user informations
	SetShouldTokenRefresh(bool)
	// ShouldTokenRefresh returns whether we should update the AuthToken with new user informations
	ShouldTokenRefresh() bool
	// Identity returns the user of the current transaction
	Identity() model.Identity
	// Context return the context
	Context() *gin.Context
	// WithTransaction wrap dao functions in a transaction
	WithTransaction(txFunc) (interface{}, error)
	// SetLang set default language for each request
	SetLang(string)
	// GetLang get current language of request scope
	GetLang() string
}

// requestScope
type requestScope struct {
	now       time.Time // the time when the request is being processed
	requestID string    // an ID identifying one or multiple correlated HTTP requests
	rollback  bool      // whether to roll back the current transaction
	db        *dbx.DB   // current DB connection
	// the executor, will be used inside daos functions
	// may be a db connection or  a transaction
	ex           interface{}
	identity     model.Identity
	tokenRefresh bool //
	context      *gin.Context
	language     string
}

// UserID return id user in scope
//func (rs *requestScope) UserID() uuid.UUID {
//	return rs.identity.GetID()
//}

// SetIdentity config identity to scope
func (rs *requestScope) SetIdentity(user model.Identity) {
	rs.identity = user
}

// SetIdentity config identity to scope
func (rs *requestScope) ShouldTokenRefresh() bool {
	return rs.tokenRefresh
}

// ShouldTokenRefresh if the parameter = true the token will be refresh
func (rs *requestScope) SetShouldTokenRefresh(refresh bool) {
	rs.tokenRefresh = refresh
}

// Identity return identity in scope
func (rs *requestScope) Identity() model.Identity {
	return rs.identity
}

// RequestID return ID request
func (rs *requestScope) RequestID() string {
	return rs.requestID
}

// SetDB sets database connection
func (rs *requestScope) SetDB(db *dbx.DB) {
	rs.db = db
}

func (rs *requestScope) Executor() dbx.Builder {
	return rs.ex.(dbx.Builder)
}

func (rs *requestScope) SetExcutor(ex interface{}) {
	rs.ex = ex.(dbx.Builder)
}

func (rs *requestScope) beginTx() *dbx.Tx {
	tx, err := rs.db.Begin()
	if err != nil {
		panic(err)
	}

	return tx
}

// Rollback return RollBack in scope
func (rs *requestScope) Rollback() bool {
	return rs.rollback
}

// SetRollback put rollback
func (rs *requestScope) SetRollback(v bool) {
	rs.rollback = v
}

// Now return resquest time
func (rs *requestScope) Now() time.Time {
	return rs.now
}

// Context return the context
func (rs *requestScope) Context() *gin.Context {
	return rs.context
}

// txFunc is a function that will be called with an initialized `Transaction` object that can be used for executing statements and queries against a database.
type txFunc func() (interface{}, error)

// WithTransaction creates a new transaction and handles rollback/commit based on the
// error object returned by the `TxFn`
// Defer stack with setExecutor running first to make sure the executor is set back to DB even the tx is commit/rollback
func (rs *requestScope) WithTransaction(fn txFunc) (result interface{}, err error) {
	// begin a Transaction
	tx := rs.beginTx()

	// now our executor is a db transaction
	rs.SetExcutor(tx)

	// This func run after the tx commit/rollback
	defer rs.SetExcutor(rs.db)

	// This func run first on defer stack to make sure tx is commit/rollback
	defer func() {
		if p := recover(); p != nil {
			// a panic occurred, rollback and repanic
			panic(p)
		} else if err != nil {
			// something went wrong, rollback
			_ = tx.Rollback()
		} else {
			// all good, commit
			_ = tx.Commit()
		}
	}()

	result, err = fn()
	if err != nil {
		return nil, err
	}

	return result, nil
}

// SetLang set default language for each request
func (rs *requestScope) SetLang(lang string) {
	rs.language = lang
}

// GetLang return current language or
func (rs *requestScope) GetLang() string {
	return rs.language
}

// newRequestScope creates a new RequestScope with the current request information.
func newRequestScope(now time.Time, logger *logrus.Logger, request *http.Request, context *gin.Context) RequestScope {
	requestID := request.Header.Get("X-Request-Id")
	return &requestScope{
		now:          now,
		requestID:    requestID,
		tokenRefresh: false,
		context:      context,
	}
}

// NewRequestScopeTest creates a new RequestScope for testing purpose
func NewRequestScopeTest(now time.Time, logger *logrus.Logger, request *http.Request, context *gin.Context) RequestScope {
	return newRequestScope(now, logger, request, context)
}

// NewEmptyRequestScope creates an empty request scope for temporary use in gRPC server
func NewEmptyRequestScope(db *dbx.DB) RequestScope {
	rs := &requestScope{}
	rs.SetDB(db)
	rs.SetExcutor(db)

	return rs
}
