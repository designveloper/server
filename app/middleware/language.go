package middleware

import (
	"github.com/gin-gonic/gin"

	"bitbucket.org/designveloper/mekong-api/app"
)

const (
	EngLang = "en"
	VieLang = "vi"
)

// SaveRequestLanguage is middleware to collect request language from context and save to request scope
func SaveRequestLanguage() gin.HandlerFunc {
	return func(c *gin.Context) {
		requestScope := app.GetRequestScope(c)

		language := c.GetHeader("Content-Language")

		switch language {
		case VieLang:
			requestScope.SetLang(VieLang)
		case EngLang:
			requestScope.SetLang(EngLang)
		default:
			requestScope.SetLang(EngLang)
		}

		c.Next()
	}
}
