package middleware

import (
	"encoding/json"
	"errors"
	"fmt"
	"strings"

	"bitbucket.org/designveloper/mekong-api/app"
	"bitbucket.org/designveloper/mekong-api/app/domain/model"
	appErrors "bitbucket.org/designveloper/mekong-api/app/errors"
	appConfig "bitbucket.org/designveloper/mekong-api/config"
	jwtGo "github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
)

// AuthJWT authJWT model
type AuthJWT struct {
	TokenLookup string
}

func jwtFromHeader(c *gin.Context, key string) (string, error) {
	authHeader := c.Request.Header.Get(key)

	if authHeader == "" {
		return "", errors.New("auth header is empty")
	}

	parts := strings.SplitN(authHeader, " ", 2)
	//if !(len(parts) == 2 && parts[0] == mw.TokenHeadName) {
	//	return "", ErrInvalidAuthHeader
	//}
	return parts[1], nil
}

// AuthToken Middleware to verify token from request
func (a AuthJWT) AuthToken() gin.HandlerFunc {
	return func(c *gin.Context) {
		// Extract token string from request header
		tokenString, err := jwtFromHeader(c, a.TokenLookup)
		if err != nil {
			_ = c.Error(errors.New("empty")).SetMeta(appErrors.Unauthorized(err, "No header token found"))
			c.Abort()
			return
		}

		jwtToken, err := jwtGo.Parse(tokenString, func(token *jwtGo.Token) (i interface{}, e error) {
			// Checking signed method
			if _, ok := token.Method.(*jwtGo.SigningMethodHMAC); !ok {
				return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
			}

			var userIdentity model.UserIdentity
			encodedJSONIdentity := token.Claims.(jwtGo.MapClaims)["identity"].(string)

			if err := json.Unmarshal([]byte(encodedJSONIdentity), &userIdentity); err != nil {
				return nil, appErrors.AuthUnableToFetchToken(nil, "")
			}
			// Attach user identity to current request context
			app.GetRequestScope(c).SetIdentity(userIdentity)

			// Return signed secret key if authorized
			return []byte(appConfig.Config.JwtSignedSecret), nil
		})

		if !jwtToken.Valid {
			_ = c.Error(errors.New("empty")).SetMeta(appErrors.Unauthorized(err, "Token is expired."))
			c.Abort()
			return
		}

		c.Set("JWT_PAYLOAD", jwtToken.Claims)
		c.Next()
	}
}
