package middleware

import (
	"net/http"

	httpRes "bitbucket.org/designveloper/mekong-api/core/http_response"
	"github.com/gin-gonic/gin"
)

type appError struct {
	Code    int    `json:"code"`
	Message string `json:"message"`
}

// JSONAppErrorReporter is middleware to collect error from context and response
func JSONAppErrorReporter() gin.HandlerFunc {
	return jsonAppErrorReporter(gin.ErrorTypeAny)
}

func jsonAppErrorReporter(errorType gin.ErrorType) gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Next()
		// after request
		detectedErrors := c.Errors.ByType(errorType)
		if len(detectedErrors) > 0 {
			var appErr appError
			switch detectedErrors[0].Meta.(type) {
			case httpRes.ErrorContainer:
				{
					errorContainer := detectedErrors[0].Meta.(httpRes.ErrorContainer)
					c.IndentedJSON(errorContainer.Status, errorContainer)
					c.Abort()
					return
				}
			default:
				appErr = appError{
					Code:    http.StatusInternalServerError,
					Message: "Internal Server Error.",
				}
			}
			// Put the error into response
			c.IndentedJSON(appErr.Code, appErr)
			c.Abort()
			return
		}
	}
}
