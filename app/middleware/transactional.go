package middleware

import (
	"bitbucket.org/designveloper/mekong-api/app"
	"github.com/gin-gonic/gin"
	dbx "github.com/go-ozzo/ozzo-dbx"
)

// Transactional is middleware to pass db connection instance into request context
func Transactional(db *dbx.DB) gin.HandlerFunc {
	return func(c *gin.Context) {
		rs := app.GetRequestScope(c)

		// set default db connection pool
		rs.SetDB(db)

		// set default executor to default db connection
		rs.SetExcutor(db)
	}
}
