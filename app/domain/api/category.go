package api

import (
	"net/http"
	"strconv"

	"bitbucket.org/designveloper/mekong-api/app"
	"bitbucket.org/designveloper/mekong-api/app/domain/model"
	"bitbucket.org/designveloper/mekong-api/app/util"

	appErrors "bitbucket.org/designveloper/mekong-api/app/errors"
	"github.com/gin-gonic/gin"
)

type categoryService interface {
	// GetCategoriesByLevel return all categories by level
	GetCategoriesByLevel(rs app.RequestScope, level int) (*[]model.Category, error)
	// GetCategories return all categories level 1 and level 2
	GetCategories(rs app.RequestScope) (*[]model.CategoryResponse, error)
	// GetCategoryPathByID return category path by id
	GetCategoryPathByID(rs app.RequestScope, catID int) (*[]model.Category, error)
	// GetSubCategoriesByCatID return sub category by id
	GetSubCategoriesByCatID(rs app.RequestScope, catID int) (*[]model.Category, error)
	// GetCategoryByCatID return category data
	GetCategoryByCatID(rs app.RequestScope, catID int) (*model.CategoryResponse, error)
	// GetRelatedCategories return category data
	GetRelatedCategories(rs app.RequestScope, catID int) (*[]model.Category, error)
}

type categoryResource struct {
	categoryService categoryService
	productService  productService
}

// ServeAuthAPIs sets up the routing of category endpoints and the corresponding handlers.
func ServeCategoryAPIs(rg gin.RouterGroup, categoryService categoryService, productService productService) {
	resource := &categoryResource{
		categoryService: categoryService,
		productService:  productService,
	}

	gr := rg.Group("/categories")
	gr.GET("", resource.getCategories)
	gr.GET("/:id", resource.getCategory)
	gr.GET("/:id/path", resource.getCategoryPath)
	gr.GET("/:id/sub", resource.getSubCategory)
	gr.GET("/:id/products", resource.getProductsInCat)
	gr.GET("/:id/related-categories", resource.getRelatedCategories)
}

func (r *categoryResource) getCategories(c *gin.Context) {
	rs := app.GetRequestScope(c)
	response, err := r.categoryService.GetCategories(rs)
	if err != nil {
		appErrors.Wrap(c, appErrors.InternalServerError(err, "Can not get categories."))
		return
	}
	c.JSON(http.StatusOK, response)
}

func (r *categoryResource) getCategoryPath(c *gin.Context) {
	rs := app.GetRequestScope(c)
	catID := util.ParseInt(c.Param("id"), 0)
	response, err := r.categoryService.GetCategoryPathByID(rs, catID)
	if err != nil {
		appErrors.Wrap(c, appErrors.InternalServerError(err, "Can not get category path."))
		return
	}
	c.JSON(http.StatusOK, response)
}

func (r *categoryResource) getSubCategory(c *gin.Context) {
	rs := app.GetRequestScope(c)
	catID := util.ParseInt(c.Param("id"), 0)
	response, err := r.categoryService.GetSubCategoriesByCatID(rs, catID)
	if err != nil {
		appErrors.Wrap(c, appErrors.InternalServerError(err, "Can not get sub category"))
		return
	}
	c.JSON(http.StatusOK, response)
}

func (r *categoryResource) getCategory(c *gin.Context) {
	rs := app.GetRequestScope(c)
	catID := util.ParseInt(c.Param("id"), 0)
	response, err := r.categoryService.GetCategoryByCatID(rs, catID)
	if err != nil {
		appErrors.Wrap(c, appErrors.InternalServerError(err, "Can not get category"))
		return
	}
	c.JSON(http.StatusOK, response)
}

func (p *categoryResource) getProductsInCat(c *gin.Context) {
	rs := app.GetRequestScope(c)

	catID, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		appErrors.Wrap(c, appErrors.InternalServerError(err, "Invalid category."))
	}
	// TODO: validate catID
	var queryRequest model.QueryRequest
	err = model.NewQueryRequest(c, &queryRequest)

	if err != nil {
		appErrors.Wrap(c, appErrors.InternalServerError(err, "Failed to get products in category."))
	}
	// Get list products
	response, err := p.productService.GetAllProductsInCat(rs, catID, queryRequest)
	if err != nil {
		appErrors.Wrap(c, appErrors.InternalServerError(err, "Failed to get products in category."))
	}
	c.JSON(http.StatusOK, response)
}

func (p *categoryResource) getRelatedCategories(c *gin.Context) {
	rs := app.GetRequestScope(c)

	catID, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		appErrors.Wrap(c, appErrors.InternalServerError(err, "Invalid category."))
	}

	cats, err := p.categoryService.GetRelatedCategories(rs, catID)
	if err != nil {
		appErrors.Wrap(c, appErrors.InternalServerError(err, "Failed to get related categories."))
	}

	c.JSON(http.StatusOK, cats)
}
