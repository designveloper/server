package api

import (
	"net/http"

	"github.com/gin-gonic/gin"

	"bitbucket.org/designveloper/mekong-api/app"
	"bitbucket.org/designveloper/mekong-api/app/domain/model"
	appErrors "bitbucket.org/designveloper/mekong-api/app/errors"
)

type searchService interface {
	// MainSearch main search bar
	MainSearch(rs app.RequestScope, search model.SearchRequest) (model.SearchResponse, error)
}

type searchResource struct {
	searchService searchService
}

// ServeSearchAPIs sets up the routing of search endpoints and the corresponding handlers.
func ServeSearchAPIs(rg gin.RouterGroup, searchService searchService) {
	resource := &searchResource{
		searchService: searchService,
	}

	// Create search api handlers
	searchGr := rg.Group("/search")
	searchGr.GET("/", resource.mainSearch)
}

func (p *searchResource) mainSearch(c *gin.Context) {
	rs := app.GetRequestScope(c)
	var searchReq model.SearchRequest

	if err := c.ShouldBindQuery(&searchReq); err != nil {
		appErrors.Wrap(c, appErrors.BadRequest(err, "Failed to parse SearchRequest model."))
		return
	}

	res, err := p.searchService.MainSearch(rs, searchReq)
	if err != nil {
		appErrors.Wrap(c, appErrors.BadRequest(err, "Failed to search."))
		return
	}

	c.JSON(http.StatusOK, res)
}
