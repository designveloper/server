package api

import (
	"net/http"

	"github.com/gin-gonic/gin"
	validation "github.com/go-ozzo/ozzo-validation"
	"github.com/go-ozzo/ozzo-validation/is"

	"bitbucket.org/designveloper/mekong-api/app"
	"bitbucket.org/designveloper/mekong-api/app/domain/model"
	appErrors "bitbucket.org/designveloper/mekong-api/app/errors"
)

type userService interface {
	// Register a new user
	RegisterUser(rs app.RequestScope, userRegister model.UserRegister) error
	// ActivateUserAccount activate an user account
	ActivateUserAccount(rs app.RequestScope, activateChallenge model.ActivateChallenge) (*model.AuthResponse, error)
	// RequestResetPassword send a link to user's email to update new password
	RequestResetPassword(rs app.RequestScope, email string) error
	// SetNewPassword set new password of user
	SetNewPassword(rs app.RequestScope, password, token string) error
}

type userResource struct {
	userService userService
}

// ServeUserAPIs sets up the routing of user endpoints and the corresponding handlers.
func ServeUserAPIs(rg gin.RouterGroup, userService userService) {
	resource := &userResource{
		userService: userService,
	}

	// Create user api handlers
	r := rg.Group("/user")
	r.POST("/register", resource.registerUser)
	r.POST("/request-reset-password", resource.requestResetPassword)
	r.POST("/reset-password", resource.resetPassword)

	r.GET("/:userID", resource.activateAccount)
}

func (r *userResource) registerUser(c *gin.Context) {
	rs := app.GetRequestScope(c)

	var userRegister model.UserRegister
	if err := c.ShouldBind(&userRegister); err != nil {
		appErrors.Wrap(c, appErrors.BadRequest(err, "failed to read userRegister from request."))
		return
	}

	if err := userRegister.Validate(); err != nil {
		errMsg := userRegister.BuildErrorMessage(err)
		appErrors.Wrap(c, appErrors.BadRequest(err, "request body has invalid properties.").SetResponseMessage(nil, errMsg))
		return
	}

	err := r.userService.RegisterUser(rs, userRegister)
	if err != nil {
		appErrors.Wrap(c, appErrors.InternalServerError(err, "Failed to register user."))
		return
	}

	c.JSON(http.StatusOK, appErrors.SuccessRequest())
}

func (r *userResource) activateAccount(c *gin.Context) {
	rs := app.GetRequestScope(c)

	var activateChallenge model.ActivateChallenge
	if err := c.ShouldBindQuery(&activateChallenge); err != nil {
		appErrors.Wrap(c, appErrors.BadRequest(err, "failed to parse activateChallenge model."))
		return
	}

	if err := activateChallenge.Validate(); err != nil {
		errMsg := activateChallenge.BuildErrorMessage(err)
		appErrors.Wrap(c, appErrors.BadRequest(err, "request param has invalid properties.").
			SetResponseMessage(nil, errMsg))
		return
	}

	res, err := r.userService.ActivateUserAccount(rs, activateChallenge)
	if err != nil {
		appErrors.Wrap(c, appErrors.InternalServerError(err, "failed to activate user."))
		return
	}

	c.JSON(http.StatusOK, res)
}

func (r *userResource) requestResetPassword(c *gin.Context) {
	rs := app.GetRequestScope(c)

	resetPassword := struct {
		Email string `json:"email"`
	}{}

	if err := c.ShouldBind(&resetPassword); err != nil {
		appErrors.Wrap(c, appErrors.BadRequest(nil, "Missing required Email field"))
		return
	}

	if validation.Validate(resetPassword.Email, is.Email) != nil {
		appErrors.Wrap(c, appErrors.BadRequest(nil, "Invalid Email"))
		return
	}

	err := r.userService.RequestResetPassword(rs, resetPassword.Email)
	if err != nil {
		appErrors.Wrap(c, appErrors.InternalServerError(err, "Fail to send password-reset link."))
		return
	}

	c.JSON(http.StatusOK, appErrors.SuccessRequest())
}

func (r *userResource) resetPassword(c *gin.Context) {
	rs := app.GetRequestScope(c)

	token := c.Query("token")
	if err := validation.Validate(token, validation.Required); err != nil {
		appErrors.Wrap(c, appErrors.BadRequest(err, "Missing required Token field"))
		return
	}

	resetPassword := model.ResetPasswordChallenge{}

	if err := c.ShouldBind(&resetPassword); err != nil {
		appErrors.Wrap(c, appErrors.BadRequest(nil, "Missing required field"))
		return
	}

	if err := resetPassword.Validate(); err != nil {
		appErrors.Wrap(c, appErrors.BadRequest(err, "Validation fail"))
		return
	}

	// set new password
	err := r.userService.SetNewPassword(rs, resetPassword.Password, token)
	if err != nil {
		appErrors.Wrap(c, appErrors.InternalServerError(err, "Cannot set new password"))
		return
	}

	c.JSON(http.StatusOK, appErrors.SuccessRequest())
}
