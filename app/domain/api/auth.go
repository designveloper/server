package api

import (
	"net/http"

	"bitbucket.org/designveloper/mekong-api/app"
	"bitbucket.org/designveloper/mekong-api/app/middleware"
	"bitbucket.org/designveloper/mekong-api/app/perm"

	"bitbucket.org/designveloper/mekong-api/app/domain/model"
	appErrors "bitbucket.org/designveloper/mekong-api/app/errors"
	"github.com/gin-gonic/gin"
)

type authService interface {
	// Authenticate authenticate user account
	Authenticate(rs app.RequestScope, auth model.AuthChallenge) (*model.AuthResponse, error)
	// RefreshToken inquire a new token
	RefreshToken(rs app.RequestScope) (*model.AuthResponse, error)
}

type authResource struct {
	authService authService
}

// ServeAuthAPIs sets up the routing of auth endpoints and the corresponding handlers.
func ServeAuthAPIs(rg gin.RouterGroup, authService authService) {
	resource := &authResource{
		authService: authService,
	}

	// Create auth api handlers
	gr := rg.Group("/")
	gr.POST("/authenticate", resource.authenticate)

	authJwt := middleware.AuthJWT{
		TokenLookup: "Authorization",
	}
	authGr := rg.Group("/")
	authGr.Use(authJwt.AuthToken())

	authenticated := perm.IsAuthenticate(authGr)
	authenticated.POST("/refresh_token", resource.refreshToken)
}

func (r *authResource) authenticate(c *gin.Context) {
	rs := app.GetRequestScope(c)
	var authChallenge model.AuthChallenge
	if err := c.ShouldBind(&authChallenge); err != nil {
		appErrors.Wrap(c, appErrors.InternalServerError(err, "Failed to read request body"))
		return
	}

	if err := authChallenge.Validate(); err != nil {
		errMsg := authChallenge.BuildErrorMessage(err)
		appErrors.Wrap(c, appErrors.InternalServerError(err, "Request body has invalid properties.").
			SetResponseMessage(nil, errMsg))
		return
	}

	response, err := r.authService.Authenticate(rs, authChallenge)
	if err != nil {
		appErrors.Wrap(c, appErrors.Unauthorized(err, "Unauthorized."))
		return
	}
	c.JSON(http.StatusOK, response)
}

func (r *authResource) refreshToken(c *gin.Context) {
	rs := app.GetRequestScope(c)

	response, err := r.authService.RefreshToken(rs)
	if err != nil {
		appErrors.Wrap(c, appErrors.Unauthorized(err, "Unauthorized."))
		return
	}
	c.JSON(http.StatusOK, response)
}
