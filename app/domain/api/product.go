package api

import (
	"net/http"

	"github.com/gin-gonic/gin"

	"bitbucket.org/designveloper/mekong-api/app"
	"bitbucket.org/designveloper/mekong-api/app/domain/model"
)

type productService interface {
	// Get get
	Get(rs app.RequestScope) (string, error)
	// GetAllProductsInCat get all products in a category
	GetAllProductsInCat(rs app.RequestScope, catID int, queryRequest model.QueryRequest) (*model.Paginate, error)
}

type productResource struct {
	productService productService
}

// ServeProductAPIs sets up the routing of product endpoints and the corresponding handlers.
func ServeProductAPIs(rg gin.RouterGroup, productService productService) {
	resource := &productResource{
		productService: productService,
	}

	// Create product api handlers
	productGr := rg.Group("/products")
	productGr.GET("/", resource.getProductsInCat)
}

func (p *productResource) getProductsInCat(c *gin.Context) {
	c.String(http.StatusOK, "catID")
}
