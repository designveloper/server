package service

import (
	"bitbucket.org/designveloper/mekong-api/app"
	"bitbucket.org/designveloper/mekong-api/app/domain/model"
)

type productRepository interface {
	CreateAttributeSet(rs app.RequestScope) error
	// GetProducts get all products in a category
	GetProducts(rs app.RequestScope, queryRequest model.QueryRequest, catIDs ...interface{}) ([]model.ProductQuery, error)
}

type ProductService struct {
	productRepo  productRepository
	categoryRepo categoryRepository
}

func NewProductService(productRepo productRepository, categoryRepo categoryRepository) *ProductService {
	return &ProductService{
		productRepo:  productRepo,
		categoryRepo: categoryRepo,
	}
}

// GetAllProductsInCat get all products in a category level
func (s ProductService) GetAllProductsInCat(rs app.RequestScope, catID int, queryRequest model.QueryRequest) (*model.Paginate, error) {
	// Get path category
	categoryPath, err := s.categoryRepo.GetCategoryPathByID(rs, catID)
	if err != nil {
		return nil, err
	}
	catIDs := make([]interface{}, len(*categoryPath))
	for i, cat := range *categoryPath {
		catIDs[i] = cat.ID
	}
	// search products
	products, err := s.productRepo.GetProducts(rs, queryRequest, catIDs...)
	if err != nil {
		return nil, err
	}

	var totalCount int
	if len(products) > 0 {
		totalCount = products[0].TotalCount
	}
	res := model.NewPaginate(
		queryRequest.Page,
		queryRequest.PerPage,
		totalCount,
		products)
	return &res, nil
}

func (s ProductService) Get(rs app.RequestScope) (string, error) {
	_, err := rs.WithTransaction(func() (i interface{}, e error) {
		err := s.productRepo.CreateAttributeSet(rs)
		return nil, err
	})

	if err != nil {
		return "", err
	}
	return "INSERT OK", nil
}
