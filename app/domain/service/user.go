package service

import (
	"errors"
	"fmt"
	"time"

	"github.com/dchest/passwordreset"
	"github.com/lib/pq"
	uuid "github.com/satori/go.uuid"
	"github.com/sirupsen/logrus"

	"bitbucket.org/designveloper/mekong-api/app"
	"bitbucket.org/designveloper/mekong-api/app/domain/model"
	appErrors "bitbucket.org/designveloper/mekong-api/app/errors"
	"bitbucket.org/designveloper/mekong-api/app/perm"
	"bitbucket.org/designveloper/mekong-api/app/util"
	appConfig "bitbucket.org/designveloper/mekong-api/config"
	"bitbucket.org/designveloper/mekong-api/core/http_response/messages"
)

const (
	userDebugMessage = "userDebugMessage"
)

type userRepository interface {
	// Create create a user
	Create(rs app.RequestScope, user model.User) error
	// GetUserByUserID get a user by user ID
	GetUserByUserID(rs app.RequestScope, userID uuid.UUID, filters ...model.UserFilter) (*model.User, error)
	// GetUserByEmail get a user by user email
	GetUserByEmail(rs app.RequestScope, email string, filters ...model.UserFilter) (*model.User, error)
	// GetActivatedUserByEmail get a user by user email
	GetActivatedUserByEmail(rs app.RequestScope, email string, filters ...model.UserFilter) (*model.User, error)
	// GetUserCredential get a user credential by user email
	GetUserCredential(rs app.RequestScope, email string) (*model.UserCredential, error)
	// ActivateUserAccount activate user account
	ActivateUserAccount(rs app.RequestScope, userID uuid.UUID) error
	// SetNewPassword set new password of user
	SetNewPassword(rs app.RequestScope, userID uuid.UUID, password string) error
}

type UserService struct {
	userRepo userRepository
}

// UserService provides services related with user.
func NewUserService(userRepo userRepository) *UserService {
	return &UserService{
		userRepo: userRepo,
	}
}

// RegisterUser register a new user account
func (s UserService) RegisterUser(rs app.RequestScope, user model.UserRegister) error {
	_, err := rs.WithTransaction(func() (i interface{}, e error) {
		userID, err := uuid.NewV4()
		if err != nil {
			return nil, err
		}

		hashedPassword, _ := util.Hash(user.Password)
		activationCode := util.RandActivateCode()
		u := model.User{
			ID:             userID,
			Email:          user.Email,
			FirstName:      user.FirstName,
			LastName:       user.LastName,
			Password:       hashedPassword,
			CreatedAt:      time.Now(),
			ActivationCode: activationCode,
		}

		err = s.userRepo.Create(rs, u)
		pqErr, _ := err.(*pq.Error)
		if err != nil {
			switch pqErr.Code {
			case "23505":
				return nil, appErrors.InternalServerError(err, pqErr.Detail).
					SetResponseMessage(rs, messages.EmailAddressAlreadyExist)
			}
		}

		// get request language
		language := rs.GetLang()

		// Send activation link to registered user
		go func() {
			err = util.SendRegisterEmail(user.Email, language, activationCode)
			logrus.Warn(err)
		}()
		return nil, nil
	})

	if err != nil {
		return err
	}

	return nil
}

// ActivateUserAccount activate an user account
func (s UserService) ActivateUserAccount(rs app.RequestScope, activateChallenge model.ActivateChallenge) (*model.AuthResponse, error) {
	res, err := rs.WithTransaction(func() (i interface{}, e error) {
		existingUser, err := s.userRepo.GetUserByEmail(rs, activateChallenge.Email)
		if err != nil {
			return nil, err
		}

		// Verify activate code
		if existingUser.ActivationCode == "" && existingUser.ActivatedAt != nil {
			return nil, appErrors.InternalServerError(nil, "").
				SetResponseMessage(rs, messages.UserAccountActivatedAlready)
		}

		if activateChallenge.ActivationCode != existingUser.ActivationCode {
			return nil, appErrors.InternalServerError(nil, "").
				SetResponseMessage(rs, messages.ActivationCodeNotCorrect)
		}

		// Update activated time and remove activate code
		err = s.userRepo.ActivateUserAccount(rs, existingUser.ID)
		if err != nil {
			return nil, err
		}

		// Create response model
		userIdentity := model.UserIdentity{
			User: existingUser,
		}
		var authResponse model.AuthResponse

		authResponse.Token, err = perm.CreateToken(userIdentity)
		if err != nil {
			return nil, appErrors.Unauthorized(errors.New("failed to create token"), userDebugMessage)
		}

		authResponse.Identity = userIdentity
		return &authResponse, nil
	})

	if err != nil {
		return nil, err
	}

	authResponse, _ := res.(*model.AuthResponse)
	return authResponse, nil
}

// GetUserByEmail return data of user by email
func (s UserService) GetUserByEmail(rs app.RequestScope, email string) (*model.User, error) {
	return s.userRepo.GetUserByEmail(rs, email)
}

// RequestResetPassword send a link to user's email to update new password
func (s UserService) RequestResetPassword(rs app.RequestScope, email string) error {
	_, err := rs.WithTransaction(func() (i interface{}, e error) {
		existingUser, err := s.userRepo.GetActivatedUserByEmail(rs, email)

		if err != nil {
			return nil, appErrors.BadRequest(err, userDebugMessage).
				SetResponseMessage(rs, messages.EmailNotBelongToAnAccount)
		}

		credential, err := s.userRepo.GetUserCredential(rs, email)
		if err != nil {
			return nil, appErrors.BadRequest(err, userDebugMessage).
				SetResponseMessage(rs, messages.EmailNotBelongToAnAccount)
		}

		// NOTE: if following the specification, the token will be never expired
		// Generate reset token that expires in 24 hours * 365 days
		token := passwordreset.NewToken(email, time.Hour*24*365, []byte(credential.Password), []byte(appConfig.Config.MekongSecret))
		// get request language
		language := rs.GetLang()
		// Send email to user's mailbox
		go func() {
			_ = util.SendResetPasswordEmail(existingUser.Email, language, token)
		}()

		return nil, nil
	})

	if err != nil {
		return err
	}

	return nil
}

// SetNewPassword set new password of user
func (s UserService) SetNewPassword(rs app.RequestScope, password, token string) error {
	_, err := rs.WithTransaction(func() (i interface{}, e error) {
		// Verify reset-password token
		login, err := passwordreset.VerifyToken(token, func(login string) (bytes []byte, e error) {
			credential, err := s.userRepo.GetUserCredential(rs, login)
			if err != nil {
				return nil, appErrors.BadRequest(err, userDebugMessage).
					SetResponseMessage(rs, messages.InvalidResetPasswordToken)
			}
			return []byte(credential.Password), nil
		}, []byte(appConfig.Config.MekongSecret))

		if err != nil {
			// verification failed, don't allow password reset
			return nil, appErrors.InternalServerError(err, userDebugMessage).
				SetResponseMessage(rs, messages.InvalidResetPasswordToken)
		}

		// get user info
		existingUser, err := s.userRepo.GetUserByEmail(rs, login)
		if err != nil {
			return nil, appErrors.InternalServerError(err, userDebugMessage).
				SetResponseMessage(rs, messages.EmailNotBelongToAnAccount)
		}

		hashedNewPassword, _ := util.Hash(password)

		err = s.userRepo.SetNewPassword(rs, existingUser.ID, hashedNewPassword)
		if err != nil {
			return nil, appErrors.InternalServerError(err, userDebugMessage)
		}

		fullName := fmt.Sprintf("%s %s", existingUser.FirstName, existingUser.LastName)
		// get request language
		language := rs.GetLang()
		// Send email to user
		go func() {
			_ = util.SendResetPasswordEmailSuccess(existingUser.Email, fullName, language)
		}()

		return nil, nil
	})

	if err != nil {
		return err
	}

	return nil
}
