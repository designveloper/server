package service

import (
	"errors"

	"bitbucket.org/designveloper/mekong-api/app"
	"bitbucket.org/designveloper/mekong-api/app/domain/model"
	appErrors "bitbucket.org/designveloper/mekong-api/app/errors"
	"bitbucket.org/designveloper/mekong-api/app/perm"
	"bitbucket.org/designveloper/mekong-api/app/util"
	"bitbucket.org/designveloper/mekong-api/core/http_response/messages"
)

const (
	authDebugMessage = "auth service"
)

type authRepository interface {
	// Authenticate authenticate user account
	Authenticate(rs app.RequestScope, auth model.AuthChallenge) error
}

// AuthService AuthService
type AuthService struct {
	authRepo authRepository
	userRepo userRepository
}

// NewAuthService provides services related with auth.
func NewAuthService(authRepo authRepository, userRepo userRepository) *AuthService {
	return &AuthService{
		authRepo: authRepo,
		userRepo: userRepo,
	}
}

// Authenticate authenticate request
func (s AuthService) Authenticate(rs app.RequestScope, auth model.AuthChallenge) (*model.AuthResponse, error) {
	credential, err := s.userRepo.GetUserCredential(rs, auth.Email)
	if err != nil {
		return nil, appErrors.Unauthorized(errors.New("invalid credentials"), authDebugMessage).
			SetResponseMessage(rs, messages.EmailOrPasswordNotCorrectCheckMailAgain)
	}

	err = util.ComparePasswordMatch(auth.Password, credential.Password)
	if err != nil {
		return nil, appErrors.Unauthorized(errors.New("invalid credentials"), authDebugMessage).
			SetResponseMessage(rs, messages.EmailOrPasswordNotCorrect)
	}

	// authenticate user payload
	user, err := s.userRepo.GetUserByEmail(rs, auth.Email)
	if err != nil {
		return nil, appErrors.Unauthorized(errors.New("invalid credentials"), authDebugMessage)
	}

	if user.ActivatedAt == nil {
		return nil, appErrors.Unauthorized(errors.New("this user account is not activated yet"), authDebugMessage).SetResponseMessage(rs, messages.AccountNotVerified)
	}

	userIdentity := model.UserIdentity{
		User: user,
	}

	// Create response model
	var authResponse model.AuthResponse
	authResponse.Token, err = perm.CreateToken(userIdentity)
	if err != nil {
		return nil, appErrors.Unauthorized(errors.New("failed to create token"), authDebugMessage)
	}

	authResponse.Identity = userIdentity
	return &authResponse, nil
}

// RefreshToken inquire a new token
func (s AuthService) RefreshToken(rs app.RequestScope) (*model.AuthResponse, error) {
	userIdentity := rs.Identity()
	// Create response model
	var authResponse model.AuthResponse
	var err error
	authResponse.Token, err = perm.CreateToken(userIdentity)
	if err != nil {
		return nil, appErrors.Unauthorized(errors.New("failed to create token"), authDebugMessage)
	}

	authResponse.Identity = userIdentity
	return &authResponse, nil
}
