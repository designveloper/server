package service

import (
	"bitbucket.org/designveloper/mekong-api/app"
	"bitbucket.org/designveloper/mekong-api/app/domain/model"
	appErrors "bitbucket.org/designveloper/mekong-api/app/errors"
)

const (
	categoryDebugMessage = "category service"
	CATEGORY_LEVEL_1     = 1
)

type categoryRepository interface {
	// GetCategoriesByLevel return all categories by level
	GetCategoriesByLevel(rs app.RequestScope, level int) (*[]model.Category, error)
	// GetSubCategoriesByCatID return sub categories by catId
	GetSubCategoriesByCatID(rs app.RequestScope, catID int) (*[]model.Category, error)
	// GetNumberProductsOfCategory return number products of category
	GetNumberProductsOfCategory(rs app.RequestScope, catID int) (int, error)
	// GetCategoryPathByID return category path by id
	GetCategoryPathByID(rs app.RequestScope, catID int) (*[]model.Category, error)
	// GetCategoryByCatID return category data
	GetCategoryByCatID(rs app.RequestScope, catID int) (*model.Category, error)
	// GetRelatedCategories return category data
	GetRelatedCategories(rs app.RequestScope, catID int) (*[]model.Category, error)
}

// CategoryService CategoryService
type CategoryService struct {
	categoryRepo categoryRepository
}

// NewCategoryService provides services related with category.
func NewCategoryService(categoryRepo categoryRepository) *CategoryService {
	return &CategoryService{
		categoryRepo: categoryRepo,
	}
}

// GetCategoriesByLevel return categories by level
func (s CategoryService) GetCategoriesByLevel(rs app.RequestScope, level int) (*[]model.Category, error) {
	categories, err := s.categoryRepo.GetCategoriesByLevel(rs, level)
	if err != nil {
		return nil, appErrors.InternalServerError(err, categoryDebugMessage)
	}
	return categories, nil
}

// GetCategories return all categories level 1 and level 2
func (s CategoryService) GetCategories(rs app.RequestScope) (*[]model.CategoryResponse, error) {
	res, err := rs.WithTransaction(func() (i interface{}, e error) {
		var response []model.CategoryResponse
		mainCategories, err := s.categoryRepo.GetCategoriesByLevel(rs, CATEGORY_LEVEL_1)
		if err != nil {
			return nil, appErrors.InternalServerError(err, categoryDebugMessage)
		}

		for _, mainCategory := range *mainCategories {
			count, err := s.categoryRepo.GetNumberProductsOfCategory(rs, mainCategory.ID)
			if err != nil {
				return nil, appErrors.InternalServerError(err, categoryDebugMessage)
			}

			subCategories, err := s.categoryRepo.GetSubCategoriesByCatID(rs, mainCategory.ID)
			if err != nil {
				return nil, appErrors.InternalServerError(err, categoryDebugMessage)
			}

			category := model.CategoryResponse{
				NumberProducts: count,
				MainCategory:   mainCategory,
				SubCategories:  subCategories,
			}
			response = append(response, category)
		}
		return response, nil
	})

	if err != nil {
		return nil, err
	}
	response, _ := res.([]model.CategoryResponse)

	return &response, nil
}

// GetCategoryPathByID return category path by id
func (s CategoryService) GetCategoryPathByID(rs app.RequestScope, catID int) (*[]model.Category, error) {
	categories, err := s.categoryRepo.GetCategoryPathByID(rs, catID)
	if err != nil {
		return nil, appErrors.InternalServerError(err, categoryDebugMessage)
	}
	return categories, nil
}

// GetSubCategoriesByCatID return sub category by id
func (s CategoryService) GetSubCategoriesByCatID(rs app.RequestScope, catID int) (*[]model.Category, error) {
	categories, err := s.categoryRepo.GetSubCategoriesByCatID(rs, catID)
	if err != nil {
		return nil, appErrors.InternalServerError(err, categoryDebugMessage)
	}
	return categories, nil
}

// GetCategoryByCatID return category data
func (s CategoryService) GetCategoryByCatID(rs app.RequestScope, catID int) (*model.CategoryResponse, error) {
	res, err := rs.WithTransaction(func() (i interface{}, e error) {
		mainCategory, err := s.categoryRepo.GetCategoryByCatID(rs, catID)
		if err != nil {
			return nil, appErrors.InternalServerError(err, categoryDebugMessage)
		}

		count, err := s.categoryRepo.GetNumberProductsOfCategory(rs, catID)
		if err != nil {
			return nil, appErrors.InternalServerError(err, categoryDebugMessage)
		}

		subCategories, err := s.categoryRepo.GetSubCategoriesByCatID(rs, catID)
		if err != nil {
			return nil, appErrors.InternalServerError(err, categoryDebugMessage)
		}

		category := model.CategoryResponse{
			NumberProducts: count,
			MainCategory:   *mainCategory,
			SubCategories:  subCategories,
		}

		return category, nil
	})

	if err != nil {
		return nil, err
	}
	response, _ := res.(model.CategoryResponse)

	return &response, nil
}

// GetRelatedCategories return category data
func (s CategoryService) GetRelatedCategories(rs app.RequestScope, catID int) (*[]model.Category, error) {
	return s.categoryRepo.GetRelatedCategories(rs, catID)
}
