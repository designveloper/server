package service

import (
	"bitbucket.org/designveloper/mekong-api/app"
	"bitbucket.org/designveloper/mekong-api/app/domain/model"
)

type searchRepository interface {
	// MainSearch main search bar
	MainSearch(rs app.RequestScope, search model.SearchRequest) ([]model.SearchResult, error)
}

type SearchService struct {
	searchRepo searchRepository
}

func NewSearchService(searchRepo searchRepository) *SearchService {
	return &SearchService{
		searchRepo: searchRepo,
	}
}

// MainSearch main search bar
func (s SearchService) MainSearch(rs app.RequestScope, searchReq model.SearchRequest) (model.SearchResponse, error) {
	// Get searched results then marshall them into SearchResponse model
	searchResults, err := s.searchRepo.MainSearch(rs, searchReq)
	if err != nil {
		return model.SearchResponse{}, err
	}

	productSearchResults := make([]model.SearchResult, 0)
	catSearchResults := make([]model.SearchResult, 0)

	for _, searchResult := range searchResults {
		switch searchResult.Type {
		case "p":
			{
				productSearchResults = append(productSearchResults, searchResult)
			}
		case "c":
			{
				catSearchResults = append(catSearchResults, searchResult)
			}
		}
	}

	if len(productSearchResults) > 0 {
		catSearchResults = append(catSearchResults, productSearchResults[0])
	}

	resultsInCats := make([]model.ResultInCat, 0)
	for _, catSearchResult := range catSearchResults {
		resultsInCats = append(resultsInCats, model.ResultInCat{
			Keyword:  catSearchResult.Search,
			Type:     catSearchResult.Type,
			Category: catSearchResult.Category,
		})
	}

	searchResponse := model.SearchResponse{
		Products:      productSearchResults,
		ResultsInCats: resultsInCats,
	}

	return searchResponse, nil
}
