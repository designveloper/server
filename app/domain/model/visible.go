package model

import "strings"

type Visible struct {
	Fields []string `json:"fields"`
}

func (v Visible) String() string {
	return strings.Join(v.Fields, ", ")
}
