package model

import "github.com/gin-gonic/gin"

const (
	DefaultPageSize = 10
)

// QueryRequest  paginate request model
type QueryRequest struct {
	Page    int    `form:"page"`
	PerPage int    `form:"per_page"`
	Search  string `form:"search"`
}

func (pr QueryRequest) GetOffsetLimit() (int, int) {
	return pr.Page - 1, pr.PerPage
}

// NewQueryRequest return QueryRequest
func NewQueryRequest(c *gin.Context, queryRequest *QueryRequest) error {
	if err := c.ShouldBindQuery(queryRequest); err != nil {
		return err
	}
	if queryRequest.Page == 0 {
		queryRequest.Page = 1
	}
	if queryRequest.PerPage == 0 {
		queryRequest.PerPage = DefaultPageSize
	}

	return nil
}

// Paginate paginate
type Paginate struct {
	TotalCount int         `json:"totalCount"`
	Page       int         `json:"page"`
	PerPage    int         `json:"perPage"`
	PageCount  int         `json:"pageCount"`
	Data       interface{} `json:"data"`
}

// NewPaginate init a new pagination
func NewPaginate(page, perPage, totalCount int, data interface{}) Paginate {
	return Paginate{
		TotalCount: totalCount,
		Page:       page,
		PerPage:    perPage,
		PageCount:  totalCount / perPage,
		Data:       data,
	}
}
