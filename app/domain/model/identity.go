package model

import uuid "github.com/satori/go.uuid"

// Identity the inteface that contains methods for all entities in system.
type Identity interface {
	// GetID id of user
	GetID() uuid.UUID
	// GetFirstName first name of user
	GetFirstName() string
	// GetLastName last name of user
	GetLastName() string
	// GetEmail Email of user
	GetEmail() string
}
