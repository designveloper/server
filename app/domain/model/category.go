package model

import "time"

type Category struct {
	ID        int        `json:"id" db:"id"`
	Name      string     `json:"name" db:"name"`
	ParentID  *int       `json:"parentId,omitempty" db:"parent_id"`
	CreatedAt time.Time  `json:"createdAt,omitempty" db:"created_at"`
	UpdatedAt *time.Time `json:"updatedAt,omitempty" db:"updated_at"`
}

type CategoryResponse struct {
	NumberProducts int         `json:"numberProducts"`
	MainCategory   Category    `json:"mainCategory"`
	SubCategories  *[]Category `json:"subCategories"`
}
