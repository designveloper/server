package model

import uuid "github.com/satori/go.uuid"

type UserIdentity struct {
	*User `json:"user"`
}

// GetID return ID of user
func (u UserIdentity) GetID() uuid.UUID {
	return u.ID
}

// GetEmail return email of user
func (u UserIdentity) GetEmail() string {
	return u.Email
}

// GetFirstName first name of user
func (u UserIdentity) GetFirstName() string {
	return u.FirstName
}

// GetLastName last name of user
func (u UserIdentity) GetLastName() string {
	return u.LastName
}
