package model

import (
	"time"

	uuid "github.com/satori/go.uuid"
)

// User user in system
type User struct {
	ID             uuid.UUID  `json:"id" db:"id"`
	Email          string     `json:"email" db:"email"`
	FirstName      string     `json:"firstName" db:"first_name"`
	LastName       string     `json:"lastName" db:"last_name"`
	Password       string     `json:"password,omitempty" db:"password"`
	CreatedAt      time.Time  `json:"createdAt" db:"created_at"`
	ActivatedAt    *time.Time `json:"activatedAt,omitempty" db:"activated_at"`
	UpdatedAt      *time.Time `json:"updatedAt,omitempty" db:"updated_at"`
	LastConnection *time.Time `json:"lastConnection,omitempty" db:"last_connection"`
	ActivationCode string     `json:"activationCode,omitempty" db:"activation_code"`
}

// UserCredential UserCredential include only email and password
type UserCredential struct {
	Email    string `db:"email"`
	Password string `db:"password"`
}

// UserFilter condition to filter user
type UserFilter struct {
	Key   string
	Value interface{}
}
