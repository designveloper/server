package model

import (
	"encoding/json"
	"errors"
	"strings"
)

type SearchRequest struct {
	Keyword string `form:"keyword" binding:"required"`
}

// Build build better format to search
func (s SearchRequest) Build() string {
	return strings.TrimSpace(s.Keyword)
}

type CategorySearch struct {
	ID       int    `json:"id" db:"id"`
	Category string `json:"category" db:"category"`
}

type SearchResult struct {
	ID       int            `json:"id" db:"id"`
	Search   string         `json:"search" db:"search"`
	Type     string         `json:"type" db:"type"`
	Category CategorySearch `json:"category" db:"category"`
}

// Scan - Implement the database/sql scanner interface
func (sr *SearchResult) Scan(value interface{}) error {
	// if value is nil, false
	if value == nil {
		// set the value of the pointer yne to YesNoEnum(false)
		return nil
	}

	bytesSource, ok := value.([]byte)
	if !ok {
		return errors.New("failed to scan SearchResult")
	}

	return json.Unmarshal(bytesSource, sr)
}

type SearchResponse struct {
	Products      []SearchResult `json:"products" db:"products"`
	ResultsInCats []ResultInCat  `json:"resultInCats"`
}

type ResultInCat struct {
	Keyword  string         `json:"keyword"`
	Type     string         `json:"type"`
	Category CategorySearch `json:"foundedIn"`
}
