package model

import "time"

// Product Product model
type Product struct {
	ID          int        `json:"id" db:"id"`
	Name        string     `json:"name" db:"name"`
	SKU         string     `json:"sku" db:"sku"`
	Description string     `json:"description" db:"description"`
	BrandID     *int       `json:"brandId" db:"brand_id"`
	PriceID     *int       `json:"priceId" db:"price_id"`
	Quantity    int        `json:"quantity" db:"quantity"`
	Taxable     bool       `json:"taxable" db:"taxable"`
	CreatedAt   time.Time  `json:"createdAt" db:"created_at"`
	UpdatedAt   *time.Time `json:"updatedAt" db:"updated_at"`
}

type ProductQuery struct {
	TotalCount int `json:"-" db:"total_count"`
	*Product
}
