package model

import (
	"strings"

	validation "github.com/go-ozzo/ozzo-validation"
	"github.com/go-ozzo/ozzo-validation/is"
)

const (
	MsgErrorEmail = "Must be a valid email address"
)

// AuthResponse contain authResponse data, necessary to log in
type AuthResponse struct {
	Token    string   `json:"token"`
	Identity Identity `json:"identity,omitempty"`
}

// AuthChallenge is used to read authentication request
type AuthChallenge struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

// ActivateChallenge is used to read activation request
type ActivateChallenge struct {
	Email          string `form:"email" binding:"required"`
	ActivationCode string `form:"activation_code" binding:"required"`
}

// Validate validate AuthChallenge model
func (ac AuthChallenge) Validate() error {
	return validation.ValidateStruct(&ac,
		validation.Field(&ac.Password, validation.Required.Error("Missing required Password field"), validation.Length(6, 60)),
		validation.Field(&ac.Email, validation.Required.Error("Missing required Email field"), is.Email.Error("Invalid Email.")),
	)
}

// BuildErrorMessage Build error message for AuthChallenge model
func (ac AuthChallenge) BuildErrorMessage(error interface{}) string {
	errs, _ := error.(validation.Errors)
	var errStringArr []string
	for _, errMsg := range errs {
		errStringArr = append(errStringArr, errMsg.Error())
	}
	return strings.Join(errStringArr, "; ")
}

// UserRegister is used to read registration request info
type UserRegister struct {
	Email     string `json:"email"`
	Password  string `json:"password"`
	FirstName string `json:"firstName"`
	LastName  string `json:"lastName"`
}

//var (
//	// reHumanName check first name , last name, full name
//	reHumanName = regexp.MustCompile(`^[a-zA-Z]*[A-Za-z]$`)
//)
//
//func checkHumanName(value string) bool {
//	return reHumanName.MatchString(value)
//}
//var (
//	// isHumanName validates if a string is valid human name
//	isHumanName = validation.NewStringRule(checkHumanName, "must be a valid human name")
//)

// Validate validate UserRegister model
func (ur UserRegister) Validate() error {
	return validation.ValidateStruct(&ur,
		validation.Field(&ur.FirstName, validation.Required, validation.Length(1, 128)),
		validation.Field(&ur.LastName, validation.Required, validation.Length(1, 128)),
		validation.Field(&ur.Password, validation.Required, validation.Length(6, 128)),
		validation.Field(&ur.Email, validation.Required, is.Email),
	)
}

// BuildErrorMessage Build error message for UserRegister model
func (ur UserRegister) BuildErrorMessage(error interface{}) string {
	errs, _ := error.(validation.Errors)
	var errStringArr []string
	for errField := range errs {
		var customizeErrMsg string
		switch errField {
		case "firstName":
			customizeErrMsg = "Missing required First Name field"
		case "lastName":
			customizeErrMsg = "Missing required Last Name field"
		case "email":
			customizeErrMsg = MsgErrorEmail
		case "password":
			customizeErrMsg = "You have to enter at least 6 characters"
		}
		errStringArr = append(errStringArr, customizeErrMsg)
	}
	return strings.Join(errStringArr, "; ")
}

// Validate validate ActivateChallenge model
func (ac ActivateChallenge) Validate() error {
	return validation.ValidateStruct(&ac,
		validation.Field(&ac.ActivationCode, validation.Required, validation.Length(8, 8)),
		validation.Field(&ac.Email, validation.Required, is.Email),
	)
}

// BuildErrorMessage Build error message for ActivateChallenge model
func (ac ActivateChallenge) BuildErrorMessage(error interface{}) string {
	errs, _ := error.(validation.Errors)
	var errStringArr []string
	for errField := range errs {
		var customizeErrMsg string
		switch errField {
		case "ActivationCode":
			customizeErrMsg = "The length of Activation Code parameter must be exactly 8"
		case "Email":
			customizeErrMsg = MsgErrorEmail
		}
		errStringArr = append(errStringArr, customizeErrMsg)
	}
	return strings.Join(errStringArr, "; ")
}

// ResetPasswordChallenge is used to read new password request
type ResetPasswordChallenge struct {
	Password string `json:"password"`
}

// Validate validate ResetPasswordChallenge model
func (rpc ResetPasswordChallenge) Validate() error {
	return validation.ValidateStruct(&rpc,
		validation.Field(&rpc.Password, validation.Required, validation.Length(6, 128)),
	)
}
