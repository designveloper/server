package repository

import (
	dbx "github.com/go-ozzo/ozzo-dbx"

	"bitbucket.org/designveloper/mekong-api/app"
	"bitbucket.org/designveloper/mekong-api/app/domain/model"
)

type SearchRepository struct {
}

// NewSearchRepository create a new SearchRepository
func NewSearchRepository() *SearchRepository {
	return &SearchRepository{}
}

// MainSearch main search bar
func (repo *SearchRepository) MainSearch(rs app.RequestScope, searchReq model.SearchRequest) ([]model.SearchResult, error) {
	searchResults := make([]model.SearchResult, 0)

	q := rs.Executor().NewQuery(`
		SELECT * FROM main_search({:search})
	`)

	q.Bind(
		dbx.Params{
			"search": searchReq.Build(),
		})

	rows, err := q.Rows()
	if err != nil {
		return nil, err
	}

	for rows.Next() {
		var searchResult model.SearchResult

		err = rows.Scan(&searchResult)
		if err != nil {
			return nil, err
		}
		searchResults = append(searchResults, searchResult)
	}

	return searchResults, err
}
