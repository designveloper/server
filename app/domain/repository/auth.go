package repository

import (
	"bitbucket.org/designveloper/mekong-api/app"
	"bitbucket.org/designveloper/mekong-api/app/domain/model"
)

type AuthRepository struct{}

func NewAuthRepository() *AuthRepository {
	return &AuthRepository{}
}

// Register a new user
func (r *AuthRepository) Authenticate(rs app.RequestScope, auth model.AuthChallenge) error {
	return rs.Executor().Model(&auth).Insert()
}
