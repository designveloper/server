package repository

import (
	dbx "github.com/go-ozzo/ozzo-dbx"

	"bitbucket.org/designveloper/mekong-api/app"
	"bitbucket.org/designveloper/mekong-api/app/domain/model"
)

type CategoryRepository struct{}

// NewCategoryRepository create a new CategoryRepository
func NewCategoryRepository() *CategoryRepository {
	return &CategoryRepository{}
}

// GetCategoriesByLevel return all categories by level
func (repo *CategoryRepository) GetCategoriesByLevel(rs app.RequestScope, level int) (*[]model.Category, error) {
	var categories []model.Category
	q := rs.Executor().NewQuery("SELECT * FROM get_categories({:level})")
	q.Prepare()
	q.Bind(dbx.Params{
		"level": level,
	})
	return &categories, q.All(&categories)
}

// GetCategoryByCatID return category data
func (repo *CategoryRepository) GetCategoryByCatID(rs app.RequestScope, catID int) (*model.Category, error) {
	var category model.Category
	q := rs.Executor().NewQuery("SELECT category.* FROM category where id = {:catID}")
	q.Prepare()
	q.Bind(dbx.Params{
		"catID": catID,
	})

	err := q.One(&category)
	return &category, err
}

// GetSubCategoriesByCatID return all sub categories by categoryId
func (repo *CategoryRepository) GetSubCategoriesByCatID(rs app.RequestScope, catID int) (*[]model.Category, error) {
	var categories []model.Category
	q := rs.Executor().NewQuery("SELECT category.* FROM category where parent_id = {:catID}")
	q.Prepare()
	q.Bind(dbx.Params{
		"catID": catID,
	})

	return &categories, q.All(&categories)
}

// GetNumberProductsOfCategory return number products of category
func (repo *CategoryRepository) GetNumberProductsOfCategory(rs app.RequestScope, catID int) (int, error) {
	var count int
	q := rs.Executor().NewQuery(`SELECT count(*) FROM category_product INNER JOIN product ON  product.id = category_product.product_id WHERE {:catID} = ANY(string_to_array(product.path, '/')::int[]);`)
	q.Prepare()
	q.Bind(dbx.Params{
		"catID": catID,
	})
	if err := q.Row(&count); err != nil {
		return 0, err
	}

	return count, nil
}

// GetCategoryPathByID return category path by id
func (repo *CategoryRepository) GetCategoryPathByID(rs app.RequestScope, catID int) (*[]model.Category, error) {
	var categories []model.Category
	q := rs.Executor().NewQuery("SELECT * FROM get_category_path({:catID})")
	q.Prepare()
	q.Bind(dbx.Params{
		"catID": catID,
	})
	return &categories, q.All(&categories)
}

// GetRelatedCategories return category path by id
func (repo *CategoryRepository) GetRelatedCategories(rs app.RequestScope, catID int) (*[]model.Category, error) {
	var categories []model.Category
	q := rs.Executor().NewQuery(`SELECT c.id, c.name, c.created_at FROM category c
	WHERE c.parent_id = (SELECT category.parent_id
	FROM category
	WHERE category.id = {:cat_id})`)

	q.Prepare()
	q.Bind(dbx.Params{
		"cat_id": catID,
	})

	err := q.All(&categories)
	return &categories, err
}
