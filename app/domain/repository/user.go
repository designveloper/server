package repository

import (
	"fmt"

	dbx "github.com/go-ozzo/ozzo-dbx"
	uuid "github.com/satori/go.uuid"

	"bitbucket.org/designveloper/mekong-api/app"
	"bitbucket.org/designveloper/mekong-api/app/domain/model"
	"bitbucket.org/designveloper/mekong-api/app/util"
)

type UserRepository struct{}

func NewUserRepository() *UserRepository {
	return &UserRepository{}
}

// Create create a user
func (r *UserRepository) Create(rs app.RequestScope, user model.User) error {
	return rs.Executor().Model(&user).Insert()
}

// GetUserByUserID get user by user ID
func (r UserRepository) GetUserByUserID(rs app.RequestScope, userID uuid.UUID, filters ...model.UserFilter) (*model.User, error) {
	var user model.User
	builtFilters := util.BuildUserFilter(filters...)

	err := rs.Executor().Select(visibleUserInfo...).
		From("user").Where(
		dbx.And(dbx.HashExp{"id": userID}, builtFilters),
	).One(&user)
	if err != nil {
		return nil, err
	}

	return &user, nil
}

// GetActivatedUserByUserID get user by user ID
func (r UserRepository) GetActivatedUserByUserID(rs app.RequestScope, userID uuid.UUID, filters ...model.UserFilter) (*model.User, error) {
	var user model.User
	builtFilters := util.BuildUserFilter(filters...)

	err := rs.Executor().Select(visibleUserInfo...).
		From("user").Where(
		dbx.And(
			dbx.HashExp{"id": userID},
			builtFilters,
			dbx.Not(dbx.HashExp{"activated_at": nil}),
		),
	).One(&user)
	if err != nil {
		return nil, err
	}

	return &user, nil
}

// GetUserByEmail get user by Email
func (r UserRepository) GetUserByEmail(rs app.RequestScope, email string, filters ...model.UserFilter) (*model.User, error) {
	builtFilters := util.BuildUserFilter(filters...)
	fmt.Println(builtFilters)
	var user model.User
	err := rs.Executor().Select(visibleUserInfo...).
		From("user").Where(
		dbx.And(
			dbx.HashExp{"email": email},
			builtFilters,
		),
	).One(&user)
	if err != nil {
		return nil, err
	}

	return &user, nil
}

// GetActivatedUserByEmail get user by Email
func (r UserRepository) GetActivatedUserByEmail(rs app.RequestScope, email string, filters ...model.UserFilter) (*model.User, error) {
	builtFilters := util.BuildUserFilter(filters...)
	fmt.Println(builtFilters)
	var user model.User
	err := rs.Executor().Select(visibleUserInfo...).
		From("user").Where(
		dbx.And(
			dbx.HashExp{"email": email},
			builtFilters,
			dbx.Not(dbx.HashExp{"activated_at": nil}),
		),
	).One(&user)
	if err != nil {
		return nil, err
	}

	return &user, nil
}

// GetUserCredential get user credentials
func (r UserRepository) GetUserCredential(rs app.RequestScope, email string) (*model.UserCredential, error) {
	var userCredential model.UserCredential
	err := rs.Executor().Select("email", "password").
		From("user").Where(
		dbx.HashExp{"email": email},
	).One(&userCredential)
	if err != nil {
		return nil, err
	}

	return &userCredential, nil
}

// ActivateUserAccount activate user account
func (r UserRepository) ActivateUserAccount(rs app.RequestScope, userID uuid.UUID) error {
	_, err := rs.Executor().Update("user", dbx.Params{"activated_at": "NOW()", "activation_code": ""}, dbx.HashExp{"id": userID}).Execute()
	return err
}

// SetNewPassword set new password of user
func (r UserRepository) SetNewPassword(rs app.RequestScope, userID uuid.UUID, password string) error {
	_, err := rs.Executor().Update("user", dbx.Params{"password": password, "updated_at": "NOW()"}, dbx.HashExp{"id": userID}).Execute()
	return err
}

var visibleUserInfo = []string{
	"id",
	"email",
	"first_name",
	"last_name",
	"created_at",
	"activated_at",
	"updated_at",
	"last_connection",
	"activation_code",
}
