package repository

import (
	dbx "github.com/go-ozzo/ozzo-dbx"

	"bitbucket.org/designveloper/mekong-api/app"
	"bitbucket.org/designveloper/mekong-api/app/domain/model"
)

var visibleProduct = model.Visible{
	Fields: []string{
		"COUNT(*) OVER() total_count",
		"product.id",
		"product.sku",
		"product.name",
		"coalesce(product.description, '')",
		"coalesce(product.quantity, 0)",
		"product.taxable",
		"product.created_at",
		"product.updated_at",
	},
}

type ProductRepository struct {
}

// NewProductRepository create a new ProductRepository
func NewProductRepository() *ProductRepository {
	return &ProductRepository{}
}

// GetProducts
func (repo *ProductRepository) GetProducts(rs app.RequestScope, queryRequest model.QueryRequest, catIDs ...interface{}) ([]model.ProductQuery, error) {
	var products []model.ProductQuery

	var withTotalField = append([]string{"COUNT(*) OVER() total_count"}, visibleProduct.Fields...)
	q := rs.Executor().Select(withTotalField...).
		From("product").
		InnerJoin("category_product cp", dbx.NewExp("product.id = cp.product_id")).
		InnerJoin("category c", dbx.NewExp("cp.category_id = c.id")).
		Where(dbx.In("c.id", catIDs...)).
		Offset(int64((queryRequest.Page - 1) * queryRequest.PerPage)).
		Limit(int64(queryRequest.PerPage)).
		OrderBy("product.name ASC")

	err := q.All(&products)
	if err != nil {
		return nil, err
	}
	return products, err
}

// CreateAttributeSet
func (repo *ProductRepository) CreateAttributeSet(rs app.RequestScope) error {
	q := rs.Executor().NewQuery("INSERT INTO public.product_attribute_set(name) VALUES('ATS_MTO3')")

	_, err := q.Execute()
	if err != nil {
		return err
	}
	return nil
}
