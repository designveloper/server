#!/bin/bash
set -euox pipefail

export TAG="${PROJECT_NAME}-${NAMESPACE}-${CIRCLE_BUILD_NUM}"
export DEPLOYMENT_NAME="${PROJECT_NAME}-${NAMESPACE}"
# Print some debugging info
kubectl cluster-info
kubectl get deployment $DEPLOYMENT_NAME -n $NAMESPACE

# Patch the current deployment
# set deployment label with image: tag to make it easier to notify the change in the UI
# set revisionHistoryLimit to cleanup older replicaSets
kubectl patch deployment $DEPLOYMENT_NAME -n $NAMESPACE -p "{\"metadata\":{\"labels\":{\"image\":\"$TAG\",\"app\":\"$DEPLOYMENT_NAME\"}},\"spec\":{\"revisionHistoryLimit\":2,\"template\":{\"spec\":{\"containers\":[{\"name\":\"$DEPLOYMENT_NAME\",\"image\":\"$DOCKER_HUB_REPO:$TAG\"}]}}}}"