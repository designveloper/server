#!/bin/bash
set -euox pipefail

export TAG="1.0"
echo "> ---BUILDING---"

# GO
build_time="$(date +'%Y-%m-%d---%H:%M:%S')"
ldflags="-X bitbucket.org/designveloper/mekong-api/app.Version=$TAG -X bitbucket.org/designveloper/mekong-api/app.BuildTime=$build_time"
go_env="GOOS=linux GOARCH=386"

ls
ls core

echo "> glide install"
(curl https://glide.sh/get) | sh && glide install

echo "> building binary for api server"
if ! env $go_env go build -v -ldflags "$ldflags" -o $CIRCLE_WORKING_DIRECTORY/dist/main cmd/main/server.go; then exit 1; fi