package main

import (
	"fmt"
	"net/http"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	dbx "github.com/go-ozzo/ozzo-dbx"
	_ "github.com/lib/pq"
	"github.com/sirupsen/logrus"
	ginlogrus "github.com/toorop/gin-logrus"

	"bitbucket.org/designveloper/mekong-api/app"
	"bitbucket.org/designveloper/mekong-api/app/domain/api"
	"bitbucket.org/designveloper/mekong-api/app/domain/repository"
	"bitbucket.org/designveloper/mekong-api/app/domain/service"
	"bitbucket.org/designveloper/mekong-api/app/middleware"
	"bitbucket.org/designveloper/mekong-api/app/util"
	appConfig "bitbucket.org/designveloper/mekong-api/config"
	httpRes "bitbucket.org/designveloper/mekong-api/core/http_response"
	"bitbucket.org/designveloper/mekong-api/core/http_response/messages"
)

func init() {
	// Load config from yaml file
	if err := appConfig.LoadConfig("./config"); err != nil {
		logrus.Fatalf("Invalid application configuration: %s", err)
	}

	// Load MessagesMap for error handler
	if err := messages.LoadMessagesMap(); err != nil {
		logrus.Fatalf("Failed to load messages map file: %s", err)
	}

	// Load UED file for error handler
	if err := httpRes.LoadUED(); err != nil {
		logrus.Fatalf("Failed to load UED file: %s", err)
	}

	// Init SMTPMail
	if err := util.SetSMTPConfig(appConfig.Config.SMTPPort, appConfig.Config.SMTPMail, appConfig.Config.SMTPPassword, appConfig.Config.SMTPServer); err != nil {
		logrus.Fatalf("Failed to load SMTP settings: %s", err)
	}
}

func main() {
	// Connect database
	db, err := dbx.MustOpen("postgres", appConfig.Config.DatabaseURL)
	if err != nil {
		panic(err)
	}

	// Start server
	address := fmt.Sprintf(":%d", appConfig.Config.ServerPort)

	router := buildRouter(db)
	s := &http.Server{
		Addr:           address,
		Handler:        router,
		ReadTimeout:    appConfig.Config.ServerReadTimeout,
		WriteTimeout:   appConfig.Config.ServerWriteTimeout,
		MaxHeaderBytes: 1 << 20,
	}
	panic(s.ListenAndServe())
}

func buildRouter(db *dbx.DB) *gin.Engine {
	log := logrus.New()
	// Force log's color
	gin.ForceConsoleColor()

	// Enable production mode when server running on production
	if *appConfig.IsProduction {
		gin.SetMode(gin.ReleaseMode)
	}

	router := gin.New()

	config := cors.DefaultConfig()
	config.AllowOrigins = []string{"http://staging.mekongdepot.com", "http://localhost:3000"}

	router.Use(
		app.Init(log),
		ginlogrus.Logger(log),
		cors.New(config),
		gin.Recovery(),
		middleware.Transactional(db),
		middleware.JSONAppErrorReporter(),
		middleware.SaveRequestLanguage(),
	)

	// Serve some special routes
	router.GET("/health", func(c *gin.Context) {
		c.JSON(http.StatusOK, struct {
			Version   string
			BuildTime string
		}{
			Version:   app.Version,
			BuildTime: app.BuildTime,
		})
	})

	router.GET("/readiness", func(c *gin.Context) {
		c.String(http.StatusOK, "OK GOOD...")
	})

	rg := *router.Group("/v1")
	//publicRg := *rg

	// Create repository instances
	productRepository := repository.NewProductRepository()
	authRepository := repository.NewAuthRepository()
	userRepository := repository.NewUserRepository()
	categoryRepository := repository.NewCategoryRepository()
	searchRepository := repository.NewSearchRepository()

	// Create service instances
	productService := service.NewProductService(productRepository, categoryRepository)
	userService := service.NewUserService(userRepository)
	authService := service.NewAuthService(authRepository, userRepository)
	categoryService := service.NewCategoryService(categoryRepository)
	searchService := service.NewSearchService(searchRepository)

	// Inject service into proper apis
	api.ServeAuthAPIs(rg, authService)
	api.ServeUserAPIs(rg, userService)
	api.ServeCategoryAPIs(rg, categoryService, productService)
	api.ServeProductAPIs(rg, productService)
	api.ServeSearchAPIs(rg, searchService)

	// The resources that need authentication to use
	authJwt := middleware.AuthJWT{
		TokenLookup: "Authorization",
	}
	rg.Use(
		authJwt.AuthToken(),
	)

	return router
}
